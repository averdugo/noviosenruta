<?php
  if(@$_GET['action'] || !isset($_GET['view'])){
?>
<!doctype html>
<html>
  <head>
<?php
?>
  	<meta charset="utf-8">
  	<title id="html_site_title"><?php echo titlepage;?></title>
  	<meta name="description" content="<?php echo description; ?>">
  	<meta name="keywords" content="<?php echo keywords; ?>">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<link rel="shortcut icon" href="<?php echo res;?>icon/icon.png">
  	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>font-awesome/css/font-awesome.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>feather/style.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>bootstrap/css/bootstrap-extended.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/app.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/colors.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/vertical-menu.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/vertical-overlay-menu.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>pace/css/pace.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>prism/css/prism.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/palette-gradient.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>morris/css/morris.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>datatables/css/datatables.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>sweetalert/css/sweetalert.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/style.css">
  	<script src="<?php echo res;?>jquery/jquery.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>tether/js/tether.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>blockui/js/blockui.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>jquery-matchheight/js/jquery-matchheight.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>unison/js/unison.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>jquery-sliding-menu/js/jquery-sliding-menu.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>screenfull/js/screenfull.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>raphael/js/raphael-min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>morris/js/morris.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>datatables/js/datatables.min.js" type="text/javascript"></script>
  	<script src="<?php echo res;?>sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
  </head>
<?php
  }
?>
  <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
          <section class="flexbox-container">
            <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
              <div class="card-header bg-transparent no-border pb-0">
                <h2 class="text-xs-center mb-2" style="font-size: 10rem;">404</h2>
                <h3 class="text-uppercase text-xs-center">Page Not Found !</h3>
              </div>
              <div class="card-body">
                <div class="row py-2">
                  <div class="col-xs-12 col-sm-6 col-md-6"><a href="<?php echo url;?>" class="btn btn-primary btn-block font-small-3"><i class="ft-home"></i> Go to home</a></div>
                  <div class="col-xs-12 col-sm-6 col-md-6"><a href="javascript:history.back()" class="btn btn-danger btn-block font-small-3"><i class="ft-arrow-left"></i>  Go Back</a></div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
<?php
  if(@$_GET['action']){
?>
    <script src="<?php echo res;?>scripts/app-menu.js" type="text/javascript"></script>
    <script src="<?php echo res;?>scripts/app.min.js" type="text/javascript"></script>
  </body>
</html>
<?php
  }
?>
