<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */

  define("VERSION", "5.0", true);
  if(file_exists('./install.php')){

  	header('Location: ./install.php');
  }else if(@$_SESSION['user_role']=='1' & file_exists('./update')){

  	header('Location: ./update');
  }else{
      
    session_start();
    define('path',dirname( __FILE__ ));
    require_once (dirname( __FILE__ ).'/config.php');
    require_once (dirname( __FILE__ ).'/core/autoload.php');
  }
