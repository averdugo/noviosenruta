<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
  $language=new language();
	$id=$_SESSION['user_id'];
	$class_user=new user();
	$class_sitedata=new site();
?>
    <style>
      .profile-card-with-stats .btn-float {padding:8px 14px 4px 14px;}.profile-card-with-cover .card-profile-image{position:absolute;top:130px;width:100%;text-align:center;}.profile-card-with-cover .card-profile-image img.img-border {border:5px solid #fff;}.profile-card-with-cover .profile-card-with-cover-content {padding-top:4rem;}#user-profile .profile-with-cover .profil-cover-details {position:absolute;margin-top:-90px;}#user-profile .profile-with-cover .profil-cover-details .profile-image img.img-border {border:5px solid #fff;}#user-profile .profile-with-cover .profil-cover-details .card-title {color:#FFFFFF;text-shadow:1px 1px 4px #1B2942;}#user-profile .navbar-profile {margin-left:130px;}
    </style>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
  <?php echo $class_sitedata->sitedata('hook_before_body');?>
  <?php include 'core/modules/nav-module.php';?>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row"></div>
      <div class="content-body">
        <div id="user-profile">
          <div class="row">
            <div class="col-xs-12">
              <div class="card profile-with-cover">
                <div class="card-img-top img-fluid bg-cover height-300" style="background:url('<?php echo res;?>icon/background.jpg') 50%;"></div>
                <div class="media profil-cover-details">
                  <div class="media-left pl-2 pt-2">
                    <a class="profile-image">
                      <img src="https://www.gravatar.com/avatar/<?php echo md5($_SESSION['user_mail']);?>?s=250" class="rounded-circle img-border height-100" alt="Card image">
                    </a>
                  </div>
                  <div class="media-body media-middle row">
                    <div class="col-xs-4">
                      <h3 class="anti_select card-title" id="user_names"><?php echo $class_user->userdata($id,'name');?> <?php echo $class_user->userdata($id,'surnames');?></h3>
                    </div>
                    <div class="col-xs-8 text-xs-right">
                      <button type="button" class="btn btn-danger" onclick="javascript:location.href='<?php echo url;?>account/sessions'"><i class="ft-unlock"></i> <?php echo $language->get_translate('active_sessions');?></button>
                      <div class="anti_select btn bg-blue bg-lighten-1 hidden-lg-down"><font color="white"><?php echo $language->get_translate('profile_joined');?> <b><?php echo strftime($language->get_translate('profile_joined_form'),strtotime($class_user->userdata($id,'created_at')));?></b></font></div>
                    </div>
                  </div>
                </div>
                <nav class="navbar navbar-light navbar-profile"><h4><br></h4></nav>
              </div>
            </div>
          </div>
        </div>
        <div class="row match-height">
      		<div class="col-md-12">
      			<div class="card">
      				<div class="card-header">
      					<h4 class="card-title anti_select" id="basic-layout-form-center"><?php echo $language->get_translate('edit_profile');?></h4>
      					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      					<div class="heading-elements">
      						<ul class="list-inline mb-0">
      							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
      						</ul>
      					</div>
      				</div>
              <div class="card-body">
                <div class="card-block">
                  <form class="form" id="submit">
      							<div class="row">
      								<div class="col-md-6 offset-md-3">
      									<div class="form-body">
                          <div class="form-group">
      											<label class="anti_select"><?php echo $language->get_translate('name');?> <font color="red">*</font></label>
                            <input class="form-control" type="text" name="name" id="name" value="<?php echo $class_user->userdata($id,'name');?>" autocomplete="off">
      										</div>
                          <div class="form-group">
                            <label class="anti_select"><?php echo $language->get_translate('surnames');?> </label>
                            <input class="form-control" type="text" name="surnames" id="surnames" value="<?php echo $class_user->userdata($id,'surnames');?>" autocomplete="off">
                          </div>
                          <div class="form-group">
                            <label class="anti_select"><?php echo $language->get_translate('email');?></label>
                            <input class="form-control" type="email" value="<?php echo $class_user->userdata($id,'email');?>" readonly="">
                          </div>
                          <div class="form-group">
                            <label class="anti_select"><?php echo $language->get_translate('profile_language');?></label>
                            <select class="form-control" name="language" required="">
                              <option value="" disabled="">Selecciona un idioma</option>
                              <?php
                                $language=new language();
                                echo $language->list_language();
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="anti_select"><?php echo $language->get_translate('password');?>  **</label>
          									<input class="form-control" type="password" name="password" id="password" value="" autocomplete="off">
                          </div>
                          <div class="form-group">
                            <label class="anti_select"><?php echo $language->get_translate('re_password');?> **</label>
          									<input class="form-control" type="password" name="re_password" id="re_password" value="" autocomplete="off">
                          </div>
                          <div class="anti_select">
                            <?php echo $language->get_translate('profile_info');?>
                          </div>
      									</div>
      								</div>
      							</div>
      							<div class="anti_select form-actions center">
      								<br><button type="submit" class="btn btn-blue"><i class="fa fa-check-square-o"></i> <?php echo $language->get_translate('profile_update');?></button>
      							</div>
      						</form>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>
    </div>
  </div>
  <script>
    $('#submit').on('submit',function(e){
      e.preventDefault();
      var block_ele = $(this).closest('.card-body');
      $(block_ele).block({
        message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
        overlayCSS:{
          backgroundColor:'#fff',
          opacity:0.8,
          cursor:'wait',
        },
        css:{
          border:0,
          padding:'10px 15px',
          color:'#fff',
          width:'auto',
          backgroundColor:'#333',
        }
      });
      var form_data=new FormData(this);
      if($("#password").val() !== $("#re_password").val()){
        sweetAlert("Error","<?php echo $language->get_translate('no_passwords');?>","error");
        document.getElementById("password").value="";
        document.getElementById("re_password").value="";
      }else{
        $.ajax({
          type:'POST',
          url:'<?php echo url;?>?action=profile',
          data:form_data,
          contentType:false,
          cache:false,
          processData:false,
          success:function(form_data){
            $(block_ele).unblock();
            if(form_data=='true'){
              $('#user_names_nav').html($("#name").val()+' '+$("#surnames").val());
              $('#user_names').html($("#name").val()+' '+$("#surnames").val());
              sweetAlert("OK","<?php echo $language->get_translate('ok_profile');?>","success");
              document.getElementById("password").value="";
              document.getElementById("re_password").value="";
            }else{
              sweetAlert("Error",form_data,"error");
              document.getElementById("password").value="";
              document.getElementById("re_password").value="";
            }
          }
        });
      }
    });
  </script>
<?php } ?>
