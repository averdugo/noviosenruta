<?php include  "templates/layout.php"; ?>

<body id="body" background="img/1920x1080/01.jpg" data-spy="scroll" data-target=".header">
	<?php include $header ?>
	<div id="page">
		<?php include "templates/slider.php" ?>
		<div id="products">
            <div class="container content-lg">
                <div class="row text-center margin-b-40">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2>¿Como Funciona?</h2>
                        <p>Novios en Ruta te permite decidir exactamente cómo comenzar esta nueva aventura
                           en tu vida. ¿La luna de miel que siempre soñaste? ¿Con los fondos en tu cuenta
                           bancaria? ¡Haz lo que quieras! </p>
                    </div>
                </div>
                <!--// end row -->

                <div class="row">
                    <!-- Latest Products -->
                    <div class="col-sm-4 sm-margin-b-50">
                        <div class="margin-b-20">
                            <img class="img-responsive wow fadeIn" src="img/970x647/01.jpg" width="850" height="450" alt="Latest Products Image" data-wow-duration=".3" data-wow-delay=".1s">
                        </div>
                        <h4><a href="#">Crea tu lista de novios</a> <span class="text-uppercase margin-l-20"></span></h4>
                        <p>Elige tu luna de miel, fracciona tus regalos en partes de acuerdo al presupuesto de cada uno de los invitados.</p>

                    </div>
                    <!-- End Latest Products -->

                    <!-- Latest Products -->
                    <div class="col-sm-4 sm-margin-b-50">
                        <div class="margin-b-20">
                            <img class="img-responsive wow fadeIn" src="img/970x647/02.jpg" alt="Latest Products Image" width="850" height="450" data-wow-duration=".3" data-wow-delay=".2s">
                        </div>
                        <h4><a href="#">Comparte con tus invitados</a> <span class="text-uppercase margin-l-20"></span></h4>
                        <p>Comparte en tus invitaciones, Redes Sociales y correo electrónico que te casas!
                           Haz que tus amigos e invitados participen, regalándote una parte de tus pasajes de
                           avión, cenas románticas, contribuciones para la remodelación de tu casa, la sesión
                           de fotos de fotos de tu matri o lo que tu elijas.</p>

                    </div>
                    <!-- End Latest Products -->

                    <!-- Latest Products -->
                    <div class="col-sm-4 sm-margin-b-50">
                        <div class="margin-b-20">
                            <img class="img-responsive wow fadeIn" src="img/970x647/03.jpg" alt="Latest Products Image" width="850" height="450" data-wow-duration=".3" data-wow-delay=".3s">
                        </div>
                        <h4><a href="#">Recibe el dinero y disfrútalo</a> <span class="text-uppercase margin-l-20"></span></h4>
                        <p>A medida que juntas el dinero, te lo enviamos a tu cuenta bancaria para que lo
                           utilices cuando quieras y como quieras. ¡En Novios en Ruta… tu tienes el control!</p>

                    </div>
                    <!-- End Latest Products -->
                </div>
                <!--// end row -->
            </div>


                <!--// end row -->
        </div>
		<?php include "templates/afiliados.php" ?>
	</div>
	<?php include $footer ?>
</body>
</html>
