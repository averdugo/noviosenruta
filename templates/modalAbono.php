<!-- Modal -->
<div class="modal fade" id="modalRegalos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Solicita Una Transferencia</h4>
      </div>
      <div class="modal-body">
		  <form class="form-horizontal" action="data/giftData.php" method="post" enctype="multipart/form-data">
  			<input type="hidden" name="user_id" value="<?php echo $user['id'];?>">

            <div class="form-group">
  			  <label class="col-md-4 control-label" for="aar">Monto a Solicitar:</label>
				<div class="col-md-6">
					<input type="text" name="amount" value="" class="form-control">
				</div>
  			</div>
            <div class="form-group">
  			  <label class="col-md-4 control-label" for="aar">Descripcion</label>
				<div class="col-md-6">
					<textarea name="description" class="form-control"></textarea>
				</div>
  			</div>
			<!-- Select Basic -->
  			<div class="form-group">
  			  <label class="col-md-4 control-label" for="send"></label>
  			  <div class="col-md-4">
  			    <button id="send"  class="btn btn-primary">Agregar</button>
  			  </div>
  			</div>
  		</form>
      </div>
    </div>
  </div>
</div>
