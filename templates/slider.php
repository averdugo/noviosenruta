<div class="promo-block">
	<div class="container">
		<div class="margin-b-40">
			<h1 class="promo-block-title">Novios En Ruta</h1>
			<p class="promo-block-text">¡El regalo más importante, lo eliges tú!</p>
		</div>
		<a class="btn-theme-md btn-white-bg text-uppercase" href="loginprueba/auth/register" title="Registro">
			<i class="btn-icon icon-control-play"></i> Registrate
		</a>
		 <!--<a class="js_popup-youtube btn-theme btn-theme-md btn-white-bg text-uppercase" href="regala.html" title="Intro Video"><i class="btn-icon icon-control-play"></i> Regala</a>-->
		<a class="btn-theme btn-theme-md btn-white-bg text-uppercase" href="#" title="Regala" data-toggle="modal" data-target="#myModal">
			<i class="btn-icon icon-control-play"></i> Regala
		</a>
	</div>
</div>
