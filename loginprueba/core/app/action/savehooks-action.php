<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
      $hook_before_header=$_POST['hook_before_header'];
      $hook_before_header=addslashes($hook_before_header);
      $hook_after_header=$_POST['hook_after_header'];
      $hook_after_header=addslashes($hook_after_header);
      $hook_before_body=$_POST['hook_before_body'];
      $hook_before_body=addslashes($hook_before_body);
      $hook_after_body=$_POST['hook_after_body'];
      $hook_after_body=addslashes($hook_after_body);
      $mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      mysqli_set_charset( $mysqli, 'utf8');
      $mysqli->query("update ".DB_PREFIX."config set var='$hook_before_header' where slug='hook_before_header'");
      $mysqli->query("update ".DB_PREFIX."config set var='$hook_after_header' where slug='hook_after_header'");
      $mysqli->query("update ".DB_PREFIX."config set var='$hook_before_body' where slug='hook_before_body'");
      $mysqli->query("update ".DB_PREFIX."config set var='$hook_after_body' where slug='hook_after_body'");
      if($mysqli){
        echo 'true';
      }else{
        echo $language->get_translate('settings_update_error');
      }
    }
  }
