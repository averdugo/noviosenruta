<?php
	require_once '../libs/db.class.php';
	function redirect()
	{
		header("Location: ../userGiftList.php");
		die();
	}
	function uploadImg()
	{
		$imgName = $_FILES['img']['name'];
		$ext = pathinfo($imgName, PATHINFO_EXTENSION);
		$newName = time().'.'.$ext;
		$path =  '../img/uploads/'.$newName;
		if (move_uploaded_file($_FILES['img']['tmp_name'], $path)) {
			return $newName;
		}else{
			redirect();
		}
	}
	if ($_FILES['img']['name']) {
		$name = uploadImg();
	}else{
		$name = "";
	}

	//die(var_dump($_POST));

	DB::insert('regalos', array(
		'img' => $name,
		'name' => $_POST['r_type'],
		'description' => $_POST['description'],
		'amount' => $_POST['amount'],
		'payments' => $_POST['payments'],
		'pareja_id'=> $_POST['pareja_id']
	));

	redirect();
 ?>
