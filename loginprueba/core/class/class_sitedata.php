<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  class site
  {
    public function sitedata($data)
    {
      $return='';
      $mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      mysqli_set_charset( $mysqli, 'utf8');
      $mysqli->real_query("select * from ".DB_PREFIX."config where slug='$data'");
      $query = $mysqli->store_result();
      while ($row = $query->fetch_assoc()){
        $return=$row['var'];
      }
      return $return;
    }
  }

  $class_sitedata=new site();

  define("url", $class_sitedata->sitedata('url'), true);
  define("res", $class_sitedata->sitedata('url').'res/', true);
  define("data", $class_sitedata->sitedata('url').'data/', true);
  define("titlepage", $class_sitedata->sitedata('title'), true);
  define("description", $class_sitedata->sitedata('description'), true);
  define("keywords", $class_sitedata->sitedata('keywords'), true);
?>
