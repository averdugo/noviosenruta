<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  require_once (dirname( __FILE__ ).'/class/class_controller.php');
  require_once (dirname( __FILE__ ).'/class/class_session.php');
  require_once (dirname( __FILE__ ).'/class/class_data.php');
  require_once (dirname( __FILE__ ).'/class/class_sitedata.php');
  require_once (dirname( __FILE__ ).'/class/class_language.php');
  require_once (dirname( __FILE__ ).'/class/class_user.php');
  require_once (dirname( __FILE__ ).'/modules/header-module.php');
  require_once (dirname( __FILE__ ).'/class/class_view.php');
  require_once (dirname( __FILE__ ).'/class/class_action.php');
  require_once (dirname( __FILE__ ).'/modules/footer-module.php');
?>
