<?php
	$id=$_SESSION['user_id'];
  $language=new language();
	$class_user=new user();
	$class_sitedata=new site();
?>
		<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-dark navbar-shadow bg-gradient-x-blue">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="javascript:void(0)" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item">
							<a href="javascript:void(0)" class="anti_select navbar-brand">
								<img alt="stack admin logo" src="<?php echo res;?>icon/icon.png" style="width:25px; height:25px;" class="anti_select brand-logo">
	              <h5 class="anti_select brand-text" id="site_title"><?php echo $class_sitedata->sitedata('title');?></h5>
							</a>
						</li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a href="javascript:void(0)" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu"></i></a></li>
            </ul>
            <ul class="nav navbar-nav float-xs-right">
              <li class="nav-item hidden-sm-down"><a href="javascript:void(0)" class="nav-link nav-link-expand"><i class="ficon ft-maximize"></i></a></li>
              <li class="dropdown dropdown-language nav-item">
                <a id="dropdown-flag" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">
                  <img style="position:relative;display:inline-block;width:1.33333333em;line-height:1em" src="<?php echo url;?>core/languages/<?php echo $language->active_language();?>/<?php echo $language->active_language();?>.svg">
                </a>
                <div aria-labelledby="dropdown-flag" class="dropdown-menu">
									<?php
								    echo $language->dropdown_language();
								  ?>
                </div>
              </li>
              <li class="dropdown dropdown-user nav-item">
                <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                  <span class="avatar"><img src="https://www.gravatar.com/avatar/<?php echo md5($_SESSION['user_mail']);?>?s=85" alt="avatar"><i></i></span>
                  <span class="user-name" id="user_names_nav"><?php echo $class_user->userdata($id,'name').' '.$class_user->userdata($id,'surnames');?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="<?php echo url;?>account/profile" class="dropdown-item"><i class="ft-user"></i> <?php echo $language->get_translate('my_profile');?></a>
                  <div class="dropdown-divider"></div>
                  <a href="<?php echo url;?>?action=logout" class="dropdown-item"><i class="ft-power"></i> <?php echo $language->get_translate('log_out');?></a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <div data-scroll-to-active="false" class="main-menu menu-fixed menu-light menu-accordion menu-shadow menu-bordered">
      <div class="main-menu-content anti_select">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class="navigation-header disabled"><span><?php echo $language->get_translate('general');?></span><i data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $language->get_translate('general');?>" class="ft-minus"></i></li>
          <li class="nav-item<?php if($_GET['view']=='dashboard'){echo' active';}?>"><a href="<?php echo url;?>dashboard"><i class="ft-home"></i><span class="menu-title"><?php echo $language->get_translate('dashboard');?></span></a></li>
          <li class="nav-item<?php if($_GET['view']=='account/profile' || $_GET['view']=='account/sessions'){echo' active';}?>"><a href="<?php echo url;?>account/profile"><i class="ft-user"></i><span class="menu-title"><?php echo $language->get_translate('my_profile');?></span></a></li>
					<?php if($_SESSION['user_role']=='1'){ ?>
					<li class="navigation-header disabled"><span><?php echo $language->get_translate('administration');?></span><i data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $language->get_translate('administration');?>" class="ft-minus"></i></li>
					<li class="nav-item<?php if($_GET['view']=='editpage'){echo' active';}?>"><a href="<?php echo url;?>editpage"><i class="ft-edit"></i><span class="menu-title"><?php echo $language->get_translate('editpage');?></span></a></li>
          <li class="nav-item<?php if($_GET['view']=='users/home' || $_GET['view']=='users/edit' || $_GET['view']=='users/add'){echo' active';}?>"><a href="<?php echo url;?>users/home"><i class="ft-user-plus"></i><span class="menu-title"><?php echo $language->get_translate('users');?></span></a></li>
          <li class="nav-item<?php if($_GET['view']=='settings'){echo' active';}?>"><a href="<?php echo url;?>settings"><i class="ft-cog"></i><span class="menu-title"><?php echo $language->get_translate('settings');?></span></a></li>
          <li class="nav-item<?php if($_GET['view']=='update'){echo' active';}?>"><a href="<?php echo url;?>update"><i class="ft-refresh-cw"></i><span class="menu-title"><?php echo $language->get_translate('update');?></span></a></li>
					<?php } ?>
				</ul>
      </div>
    </div>
