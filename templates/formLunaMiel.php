<style media="screen">
	.hideSelects{
		display:none;
	}
</style>
<div id="formLunaMiel"  class="sss">
	<h2>Luna de Miel</h2>
	<form class="form-horizontal" action="data/honeyData.php" method="post">
		<input type="hidden" name="pareja_id" value="<?php echo $weddingProfile['id_newperfil'];?>">
		<input type="hidden" id="whereHide" name="where3" value="">
		<div class="form-group">
		  <label class="col-md-4 control-label" for="aar" >¿A que region Desean Viajar?</label>
		  <div class="col-md-4">
		    <select id="aar"  class="form-control wherePart">
				<option value="">Elige una opcion</option>
				<option value="1">Europa</option>
				<option value="2">Sudamérica</option>
				<option value="3">Asia</option>
				<option value="4">Viajar por nuestro pais</option>
				<option value="5">América central y México</option>
				<option value="6">Caribe</option>
				<option value="7">África</option>
				<option value="8">Oceanía</option>
		    </select>
		  </div>
		</div>

		<div id="reg1" class="hideSelects form-group">
		  <label class="col-md-4 control-label" for="aar">¿A que region de Europa desean viajar?</label>
		  <div class="col-md-4">
		    <select name="where[]" class=" form-control countrySelect" multiple="multiple">

		      <option value="Alemania">Alemania</option>
		      <option value="Francia">Francia</option>
		      <option value="España">España</option>
		      <option value="Italia">Italia</option>
		      <option value="Inglaterra">Inglaterra</option>
		      <option value="Rusia">Rusia</option>
		      <option value="Austria">Austria</option>
		      <option value="Grecia">Grecia</option>
		      <option value="Polonia">Polonia</option>
		    </select>
		  </div>
		</div>
		<div id="reg2" class="hideSelects form-group">
		  <label class="col-md-4 control-label" for="aar">¿A que region de Sudamèrica desean viajar?</label>
		  <div class="col-md-4">
		    <select  name="where[]" class=" form-control countrySelect" multiple="multiple">

		      <option value="Brasil">Brasil</option>
		      <option value="Bolivia">Bolivia</option>
		      <option value="Peru">Peru</option>
		      <option value="Colombia">Colombia</option>
		      <option value="Chile">Chile</option>
		      <option value="Ecuador">Ecuador</option>
		      <option value="Paraguay">Paraguay</option>
		      <option value="Uruguay">Uruguay</option>
		      <option value="Venezuela">Venezuela</option>
		    </select>
		  </div>
		</div>
		<div id="reg3" class="hideSelects form-group" style="display:none">
		  <label class="col-md-4 control-label" for="aar">¿A que region de Asia desean viajar?</label>
		  <div class="col-md-4">
		    <select name="where[]" class="  form-control countrySelect"  multiple="multiple">

		      <option value="India">India</option>
		      <option value="Vietnam">Vietnam</option>
		      <option value="Indonesia">Indonesia</option>
		      <option value="Taiwan">Taiwan</option>
		      <option value="Singapure">Singapure</option>
		      <option value="Corea del sur">Corea del sur</option>
		      <option value="Tailandia">Tailandia</option>
		      <option value="Japon">Japon</option>
		      <option value="China">China</option>
		    </select>
		  </div>
		</div>
		<div id="reg4" class="hideSelects form-group">
		  <label class="col-md-4 control-label" for="aar">¿A que region de Chile desean viajar?</label>
		  <div class="col-md-4">
		    <select name="where[]" class=" form-control countrySelect" multiple="multiple">

		      <option value="San Pedro de Atacama">San Pedro de Atacama</option>
		      <option value="Arica">Arica</option>
		      <option value="Iquique">Iquique</option>
		      <option value="Isla de Pascua">Isla de Pascua</option>
		      <option value="Valparaiso">Valparaiso</option>
		      <option value="Chiloe">Chiloe</option>
		      <option value="Patagonia">Patagonia</option>
		      <option value="Torres del Paine">Torres del Paine</option>
		    </select>
		  </div>
		</div>

		<!-- Select Basic -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="maaned">¿Que planes tienen para su luna de miel ?</label>
		  <div class="col-md-4">
		    <select  name="plan" class="form-control">
				<option value="">Elige una opcion</option>
		      <option value="Hacer un crucero">Hacer un crucero</option>
		      <option value="Viajar por un pais">Viajar por un pais / region</option>
		      <option value="Viajar por varios lugares">Viajar por varios lugares</option>
		    </select>
		  </div>
		</div>
		<!-- Select Basic -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="maaned">¿Cuantas noches?</label>
		  <div class="col-md-4">
		    <select  name="days" class="form-control">
		      <option value="1">01</option>
		      <option value="2">02</option>
		      <option value="3">03</option>
		      <option value="4">04</option>
		      <option value="5">05</option>
		      <option value="6">06</option>
		      <option value="7">07</option>
		      <option value="8">08</option>
		      <option value="9">09</option>
		      <option value="10">10</option>
		      <option value="11">11</option>
		      <option value="12">12</option>
		      <option value="13">13</option>
		      <option value="14">14</option>
		      <option value="15">15</option>
		    </select>
		  </div>
		</div>
		<div class="form-group">
			<label for="" class="col-md-4 control-label">Descripcion</label>
			<div class="col-md-4">
				<textarea name="description" class="form-control"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="col-md-4 control-label">Presupuesto Estimado</label>
			<div class="col-md-4">
				<input type="text" name="amount" class="form-control" value="">
			</div>
		</div>
		<div class="form-group">
		  <label class="col-md-4 control-label" for="aar">Abonos</label>
			<div class="col-md-6">
				<input type="number" name="payments" value="1" class="form-control" >
			</div>
		</div>
		<!-- Select Basic -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="send"></label>
		  <div class="col-md-4">
		    <button id="send"  class="btn btn-primary">Agregar</button>
		  </div>
		</div>

	</form>
</div>
