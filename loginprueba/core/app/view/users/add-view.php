<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'?destination=users/home";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
  $language=new language();
	$class_user=new user();
	$class_sitedata=new site();
?>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
    <?php echo $class_sitedata->sitedata('hook_before_body');?>
    <?php include 'core/modules/nav-module.php';?>
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
          <div class="row match-height">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title anti_select" id="basic-layout-form-center"><?php echo $language->get_translate('user_add');?></h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-body">
                  <div class="card-block">
                    <form class="form form-horizontal" id="submit">
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> <?php echo $language->get_translate('user_data');?></h4>
                        <div class="form-group row">
                          <label class="col-md-3 label-control"><?php echo $language->get_translate('name');?> <font color="red">*</font></label>
                          <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="<?php echo $language->get_translate('name');?>" id="name" required="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 label-control"><?php echo $language->get_translate('surnames');?></label>
                          <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="<?php echo $language->get_translate('surnames');?>" id="surnames">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 label-control"><?php echo $language->get_translate('email');?> <font color="red">*</font></label>
                          <div class="col-md-9">
                            <input type="email" class="form-control" placeholder="<?php echo $language->get_translate('email');?>" id="email" required="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 label-control"><?php echo $language->get_translate('password');?> <font color="red">*</font></label>
                          <div class="col-md-9">
                            <input type="password" class="form-control" placeholder="<?php echo $language->get_translate('password');?>" id="password" required="">
                          </div>
                        </div>
                        <h4 class="form-section"><i class="ft-unlock"></i> <?php echo $language->get_translate('user_perm');?></h4>
                        <div class="form-group row">
                          <label class="col-md-3 label-control"><?php echo $language->get_translate('user_status');?></label>
                          <div class="col-md-9">
                            <fieldset>
                              <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="status">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"><?php echo $language->get_translate('user_active');?></span>
                              </label>
                            </fieldset>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 label-control"><?php echo $language->get_translate('user_type');?> <font color="red">*</font></label>
                          <div class="col-md-9">
                            <fieldset>
                              <label class="custom-control custom-radio">
                                <input id="role" name="role" type="radio" value="1" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"><?php echo $language->get_translate('user_admin');?></span>
                              </label>
                            </fieldset>
                            <fieldset>
                              <label class="custom-control custom-radio">
                                <input id="role" name="role" type="radio" value="2" class="custom-control-input" checked="">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"><?php echo $language->get_translate('user_standard');?></span>
                              </label>
                            </fieldset>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus-square"></i> <?php echo $language->get_translate('user_add');?></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
			$('#submit').on('submit',function(e){
        var block_ele = $(this).closest('.card-body');
        $(block_ele).block({
          message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
          overlayCSS:{
            backgroundColor:'#fff',
            opacity:0.8,
            cursor:'wait',
          },
          css:{
            border:0,
            padding:'10px 15px',
            color:'#fff',
            width:'auto',
            backgroundColor:'#333',
          }
        });
        e.preventDefault();
        var name=$("#name").val();
        var surnames=$("#surnames").val();
        var email=$('#email').val();
        var password=$('#password').val();
        var role=$('#role:checked').val();
        if($("#status").is(":checked")){var status='1';}else{var status='0';}
        var dataString='name='+name+'&surnames='+surnames+'&email='+email+'&password='+password+'&role='+role+'&status='+status;
        $.ajax({
          type:'POST',
          data:dataString,
          url:'<?php echo url;?>?action=newuser',
          success:function(data){
            $(block_ele).unblock();
            if(data=='true'){
              $("#submit")[0].reset();
              sweetAlert("OK","<?php echo $language->get_translate('user_add_ok');?>","success");
            }else{
              sweetAlert("Error",data,"error");
            }
          }
        });
			});
		</script>
<?php }} ?>
