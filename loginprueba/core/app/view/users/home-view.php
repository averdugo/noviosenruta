<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'?destination=users/home";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
  $language=new language();
	$id=$_SESSION['user_id'];
	$class_user=new user();
	$class_sitedata=new site();
?>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
  <?php echo $class_sitedata->sitedata('hook_before_body');?>
  <?php include 'core/modules/nav-module.php';?>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row"></div>
      <div class="content-body">
        <div class="row match-height">
      		<div class="col-md-12">
      			<div class="card">
      				<div class="card-header">
      					<h4 class="card-title anti_select" id="basic-layout-form-center"><?php echo $language->get_translate('users');?></h4>
      					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      					<div class="heading-elements">
      						<ul class="list-inline mb-0">
                    <li><a href="<?php echo url;?>users/add" class="btn btn-secondary btn-sm"><?php echo $language->get_translate('new_user');?></a>&nbsp;&nbsp;&nbsp;&nbsp;</li>
      							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
      						</ul>
      					</div>
      				</div>
              <div class="card-body">
                <div class="card-block">
                  <form id="search_user" method="post">
                    <input type="text" class="form-control input-group-sm" id="search" placeholder="<?php echo $language->get_translate('search_users');?>" autocomplete="off">
                  </form>
                  <br><br>
                  <div class="row">
                    <div id="users"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      $("#search_user").submit();
    });
		$('#search_user').on('submit', function(e){
      var block_ele = $(this).closest('.card-body');
      $(block_ele).block({
        message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
        overlayCSS:{
          backgroundColor:'#fff',
          opacity:0.8,
          cursor:'wait',
        },
        css:{
          border:0,
          padding:'10px 15px',
          color:'#fff',
          width:'auto',
          backgroundColor:'#333',
        }
      });
      e.preventDefault();
      var search=$("#search").val();
      var dataString='search='+search;
      $.ajax({
        type:'POST',
        data:dataString,
        url:'<?php echo url;?>?action=searchuser',
        success:function(data){
          $(block_ele).unblock();
          $("#users").html(data);
        }
      });
		});
    function del_user(id){
      sweetAlert({
        title: "<?php echo $language->get_translate('are_you_sure');?>",
        text: "<?php echo $language->get_translate('del_user_info');?>",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "<?php echo $language->get_translate('yes');?>",
        cancelButtonText: "<?php echo $language->get_translate('no');?>",
        closeOnConfirm: false,
        closeOnCancel: false
        },
        function(isConfirm){
        if(isConfirm) {
          var search=$("#search").val();
          var dataString='id='+id;
          $.ajax({
            type:'POST',
            data:dataString,
            url:'<?php echo url;?>?action=deluser',
            success:function(data){
              document.getElementById("search").value="";
              $("#search_user").submit();
              if(data=='true'){
                $("#user_"+id).remove();
                sweetAlert("OK", "<?php echo $language->get_translate('del_user_ok');?>", "success");
              }else{
                sweetAlert("Error",data, "error");
              }
            }
          });
        }else{
          sweetAlert("Cancelado", "<?php echo $language->get_translate('cancel_action');?>", "error");
        }
      });
    }
	</script>
<?php }} ?>
