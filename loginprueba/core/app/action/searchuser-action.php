<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
    if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
      if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
        $language=new language();
      	$search=$_POST['search'];
      	$mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      	mysqli_set_charset( $mysqli, 'utf8');
        $mysqli->real_query("select * from ".DB_PREFIX."user where name like '%$search%' or email like '%$search%' order by id asc");
        $query=$mysqli->store_result();
        $n_rows=$query->num_rows;
        if($n_rows=='0'){
?>
  <div class="col-md-12">
    <div class="card card-inverse box-shadow-0 card-danger">
      <div class="card-header">
        <br><br><br>
        <h1><?php echo $language->get_translate('no_found_user');?></h1>
        <p><?php echo $language->get_translate('no_found_user_desc');?></p>
        <br><br><br>
			</div>
		</div>
  </div>
<?php
      }else{
        while ($row=$query->fetch_assoc()){
?>
  <div class="col-xl-3 col-md-6 col-xs-12" id="user_<?php echo $row['id'];?>">
    <div class="card box-shadow-1">
      <div class="text-xs-center">
        <div class="card-block">
          <img src="https://www.gravatar.com/avatar/<?php echo md5($row['email']);?>?s=150" class="rounded-circle height-150">
        </div>
        <div class="card-block">
          <h4 class="card-title"><?php echo $row['name'];?> <?php echo $row['surnames'];?></h4>
          <h6 class="card-subtitle text-muted"><?php echo $row['email'];?></h6><br>
          <?php if($row['role']=='1'){ ?>
          <p class="btn btn-blue btn-sm anti_select"><?php echo $language->get_translate('user_admin');?></p><br>
          <?php }else{ ?>
          <p class="btn btn-warning btn-sm anti_select"><?php echo $language->get_translate('user_standard');?></p><br>
          <?php } ?>
          <?php if($row['is_active']=='1'){ ?>
          <p class="btn btn-success btn-sm anti_select"><?php echo $language->get_translate('user_active');?></p>
          <?php }else{ ?>
          <p class="btn btn-danger btn-sm anti_select"><?php echo $language->get_translate('user_disabled');?></p>
          <?php } ?>
        </div>
        <div class="text-xs-center">
          <?php if($_SESSION['user_id']==$row['id']){ ?>
          <a href="<?php echo url;?>account/profile" class="btn btn-xs btn-success"><i class="fa fa-user"></i> <?php echo $language->get_translate('user_visit');?></a>
          <?php }else{ ?>
          <a href="<?php echo url;?>users/edit?id=<?php echo $row['id'];?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> <?php echo $language->get_translate('user_edit');?></a>
          <?php if($row['role']=='2'){ ?>
          <a href="javascript:void(0);" onclick="del_user('<?php echo $row['id'];?>');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> <?php echo $language->get_translate('user_del');?></a>
          <?php }} ?>
          <br><br>
        </div>
      </div>
    </div>
  </div>
<?php }}}} ?>
