<?php
	include  "templates/layout.php";
	session_start();
	$user = DB::queryFirstRow("SELECT * FROM usuariosuser WHERE id=%s", $_SESSION['user_id']);
	$weddingProfile = DB::queryFirstRow("SELECT * FROM pareja_newperfil WHERE user_id=%s", $_SESSION['user_id']);
	$gifts = DB::query("SELECT * FROM regalos WHERE pareja_id=%s", $weddingProfile['id_newperfil']);

?>

<body id="body" data-spy="scroll" data-target=".header">
	<?php include $header ?>

	<style media="screen">
		.sss{
			width: 80%;
			margin: 0 auto;
		}
		textarea.form-control{
			border: 1px solid;
			height: 100px
		}
		td{
			text-align: left;
		}
	</style>

	<div id="page">
		<div class="promo-block">
			<div class="container">
				<div class="row profile">
					<?php include "templates/profileSidebar.php"; ?>
					<div class="col-md-9">
						<div class="profile-content">
							<div id="formLunaMiel"  class="sss" >
								<h2>
									Lista de Regalos
									<button type="button" class="btn btn-primary pull-right" name="button" data-toggle="modal" data-target="#modalRegalos">+</button>
								</h2>
								<table class="table">
									<thead>
										<tr>
											<th>Imagen</th>
											<th>Tipo</th>
											<th>Descripcion</th>
											<th>Precio total</th>
											<th>Abonos</th>
											<th>Precio Unitario</th>

										</tr>
									</thead>
									<tbody>
										<?php foreach ($gifts as $key => $value): ?>
											<tr>
												<td><img src="img/uploads/<?php echo $value['img']; ?>" class="img-responsive" alt="" style="max-width:100px"></td>
												<td><?php echo $value['name']; ?></td>
												<td><?php echo $value['description']; ?></td>
												<td>$ <?php echo $value['amount']; ?></td>
												<td><?php echo $value['payments']; ?></td>
												<td>$ <?php
														$unit = $value['amount'] / $value['payments'];
														echo $unit;
													?>
												</td>

											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include $footer ?>
	<?php include "templates/modalGift.php"; ?>
	<script src="js/calendar.min.js"></script>


</body>
</html>
