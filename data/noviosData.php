<?php
	require_once '../libs/db.class.php';
	function redirect()
	{
		header("Location: ../userprofile.php");
		die();
	}
	function uploadImg()
	{
		$imgName = $_FILES['img']['name'];
		$ext = pathinfo($imgName, PATHINFO_EXTENSION);
		$newName = time().'.'.$ext;
		$path =  '../img/uploads/'.$newName;
		if (move_uploaded_file($_FILES['img']['tmp_name'], $path)) {
			return $newName;
		}else{
			redirect();
		}
	}
	function uploadImg2()
	{
		$imgName = $_FILES['imgFondo']['name'];
		$ext = pathinfo($imgName, PATHINFO_EXTENSION);
		$newName = time().'.'.$ext;
		$path =  '../img/uploads/'.$newName;
		if (move_uploaded_file($_FILES['imgFondo']['tmp_name'], $path)) {
			return $newName;
		}else{
			redirect();
		}
	}

	$name = "";
	$nameFondo = "";

	if ( $_POST['weddingProfile_id'] ) {

		$weddingProfile = DB::queryFirstRow("SELECT * FROM pareja_newperfil WHERE id_newperfil=%s", $_POST['weddingProfile_id'] );

		if ($_FILES['img']['name']) {
			$name = uploadImg();
		}else{
			$name = $weddingProfile['img'];
		}

		if ($_FILES['imgFondo']['name']) {
			$nameFondo = uploadImg2();
		}

		DB::insertUpdate('pareja_newperfil', array(
			'id_newperfil' => $_POST['weddingProfile_id']
		), array(
			'nom1_newperfil' => $_POST['nombreNovia'],
			'nom2_newperfil' => $_POST['nombreNovio'],
			'fechbod_newperfil' => $_POST['fechaboda'],
			'agrad_newperfil' => $_POST['comments'],
			'user_id' => $_POST['user_id'],
			'img' => $name,
			'img_fondo' => $nameFondo,
			'phone' => $_POST['phone'],
			'titular_cta' => $_POST['titular_cta'],
			'mail_cta' => $_POST['mail_cta'],
			'tipo_cta' => $_POST['tipo_cta'],
			'bank_name' => $_POST['bank_name'],
			'bank_number' => $_POST['bank_number'],
		));

		redirect();

	}else{
		if ($_FILES['img']['name']) {
			$name = uploadImg();
		}else{
			$name = "";
		}
		if ($_FILES['imgFondo']['name']) {
			$nameFondo = uploadImg2();
		}

		DB::insert('pareja_newperfil', array(
			'nom1_newperfil' => $_POST['nombreNovia'],
			'nom2_newperfil' => $_POST['nombreNovio'],
			'fechbod_newperfil' => $_POST['fechaboda'],
			'agrad_newperfil' => $_POST['comments'],
			'user_id' => $_POST['user_id'],
			'img' => $name,
			'img_fondo' => $nameFondo,
			'phone' => $_POST['phone'],
			'titular_cta' => $_POST['titular_cta'],
			'mail_cta' => $_POST['mail_cta'],
			'tipo_cta' => $_POST['tipo_cta'],
			'bank_name' => $_POST['bank_name'],
			'bank_number' => $_POST['bank_number'],
		));

		redirect();


	}





?>
