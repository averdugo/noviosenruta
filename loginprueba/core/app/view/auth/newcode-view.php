<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  if(isset($_SESSION['user_mail'])){echo'<script>window.location="'.url.'dashboard";</script>';}else if(isset($_SESSION['token'])){echo'<script>window.location="'.url.'?action=logout";</script>';}else{
  $language=new language();
	$class_sitedata=new site();
?>
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-blue bg-lighten-2 blank-page blank-page">
      <div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
            <section class="flexbox-container">
              <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                  <div class="card-header no-border">
                    <div class="card-title text-xs-center">
                      <div class="p-1">
                        <h2 style="vertical-align: middle;"><img src="<?php echo res;?>icon/icon.png" style="width:45px;height45px;" alt="Puzzle Web"> <?php echo titlepage;?></h2>
                      </div>
                    </div>
                    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                      <span><?php echo $language->get_translate('new_code');?></span>
                    </h6>
                  </div>
                  <div class="card-body">
                    <div class="card-block pt-0">
                      <form class="form-horizontal" id="submit">
                        <fieldset class="form-group floating-label-form-group">
                          <label for="user-name"><?php echo $language->get_translate('email');?> <font color="red">*</font></label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $language->get_translate('email');?>" required="">
                        </fieldset>
                        <fieldset class="form-group row"></fieldset>
                        <button type="submit" class="btn btn-outline-success btn-block"><i class="ft-refresh-ccw"></i> <?php echo $language->get_translate('send_code');?></button>
                        <a href="<?php echo url;?>auth/active" class="btn btn-outline-warning btn-block"><i class="fa fa-toggle-off"></i> <?php echo $language->get_translate('active_button');?></a>
                      </form>
                    </div>
                    <div class="card-block">
                      <a href="<?php echo url;?>" class="btn btn-outline-blue btn-block"><i class="ft-unlock"></i> <?php echo $language->get_translate('login');?></a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        $('#submit').on('submit',function(e){
          var block_ele = $(this).closest('.card-body');
          $(block_ele).block({
            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
            overlayCSS:{
              backgroundColor:'#fff',
              opacity:0.8,
              cursor:'wait',
            },
            css:{
              border:0,
              padding:'10px 15px',
              color:'#fff',
              width:'auto',
              backgroundColor:'#333',
            }
          });
          e.preventDefault();
          var form_data=new FormData(this);
          $.ajax({
            type:'POST',
            url:'<?php echo url;?>?action=newcode',
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(form_data){
              if(form_data=='ok'){
                $("#submit")[0].reset();
                sweetAlert({title:"OK",text:'<?php echo $language->get_translate('newcode_ok');?>',timer:2000,showConfirmButton:true,type:"success"},function(){setTimeout(go_to_login(),1000);});
                function go_to_login(){window.location="../";}
              }else{
                $(block_ele).unblock();
                sweetAlert({title:"Error",text:form_data,timer:1000,showConfirmButton:false,type:"error"});
                document.getElementById("code").value="";
              }
            }
          });
        });
      </script>
<?php } ?>
