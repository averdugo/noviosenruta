<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
      $allow_registers=$_POST['allow_registers'];
      $language=$_POST['language'];
      $google_active=addslashes($_POST['google_active']);
      $google_client_id=addslashes($_POST['google_client_id']);
      $google_client_secret=addslashes($_POST['google_client_secret']);
      $mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      mysqli_set_charset( $mysqli, 'utf8');
      $mysqli->query("update ".DB_PREFIX."config set var='$allow_registers' where slug='allow_registers'");
      $mysqli->query("update ".DB_PREFIX."config set var='$language' where slug='default_language'");
      $mysqli->query("update ".DB_PREFIX."config set var='$google_active' where slug='google_active'");
      $mysqli->query("update ".DB_PREFIX."config set var='$google_client_id' where slug='google_client_id'");
      $mysqli->query("update ".DB_PREFIX."config set var='$google_client_secret' where slug='google_client_secret'");
      if($mysqli){
        echo 'true';
      }else{
      	echo $language->get_translate('settings_update_error');
      }
    }
  }
