<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $class_sitedata=new site();
  $language=new language();
  include_once 'res/google_php/Google_Client.php';
  include_once 'res/google_php/contrib/Google_Oauth2Service.php';
  $gClient=new Google_Client();
  $gClient->setApplicationName('Login to '.$class_sitedata->sitedata('title'));
  $gClient->setClientId($class_sitedata->sitedata('google_client_id'));
  $gClient->setClientSecret($class_sitedata->sitedata('google_client_secret'));
  $gClient->setRedirectUri(url.'?action=register_google');
  $google_oauthV2=new Google_Oauth2Service($gClient);
  if(isset($_GET['code'])){
  	$gClient->authenticate($_GET['code']);
  	$_SESSION['token']=$gClient->getAccessToken();
  	header('Location:'.filter_var(url.'?action=register_google',FILTER_SANITIZE_URL));
  }
  if(isset($_SESSION['token'])) {
  	$gClient->setAccessToken($_SESSION['token']);
  }
  if($gClient->getAccessToken()){
  	$gpUserProfile=$google_oauthV2->userinfo->get();
  	$str="abcdefghijklmopqrstuvwxyz1234567890";
  	$code="";
  	for ($i=0; $i < 6; $i++) {
  		$code.=$str[rand(0,strlen($str)-1)];
  	}
    $name=$gpUserProfile['given_name'];
  	$surnames=$gpUserProfile['family_name'];
  	$email=$gpUserProfile['email'];
  	$password=$code;
  	$role='2';
  	$password=sha1(md5($password));
  	if(empty($surnames)){$surnames='';}
  	$mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
  	mysqli_set_charset( $mysqli, 'utf8');
  	$mysqli->real_query("select * from ".DB_PREFIX."user where email='$email'");
  	$query=$mysqli->store_result();
  	$rows=$query->num_rows;
  	if($rows=='0'){
      $mysqli->query("insert into ".DB_PREFIX."user (id, name, surnames, email, password, is_active, is_recover, role, code, image) values (null, '$name', '$surnames', '$email', '$password', 0, 0, '$role', '$code', null)");
      $subject='Bienvenido a '.$class_sitedata->sitedata('title');
      $message=$class_sitedata->sitedata('email_register');
      $message=str_replace("[sitename]", $class_sitedata->sitedata('title'), $message);
      $message=str_replace("[username]", $email, $message);
      $message=str_replace("[activecode]", $code, $message);
      $headers='MIME-Version: 1.0' . "\r\n";
      $headers.='Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $headers.='From: '.$class_sitedata->sitedata("title").' <'.$class_sitedata->sitedata("site_mail").'>' . "\r\n";
      @mail($email, $subject, $message, $headers);
      $file=fopen("data/actions/register_".md5($email).".txt","w");
  		fwrite($file,$message);
  		fclose($file);
  		echo'<script>alert("'.$language->get_translate('social_register_ok').'");window.location="'.url.'auth/register";</script>';
  	}else{
  		echo'<script>alert("'.$language->get_translate('social_repeat_user').'");window.location="'.url.'";</script>';
  	}
  }else{
    $authUrl=$gClient->createAuthUrl();
    header('Location:'.filter_var($authUrl,FILTER_SANITIZE_URL));
  }
