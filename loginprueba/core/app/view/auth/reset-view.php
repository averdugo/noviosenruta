<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  if(isset($_SESSION['user_mail'])){echo'<script>window.location="'.url.'dashboard";</script>';}else if(isset($_SESSION['token'])){echo'<script>window.location="'.url.'?action=logout";</script>';}else{
  $code=$_GET['code'];
  $mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	mysqli_set_charset( $mysqli, 'utf8');
	$mysqli->real_query("select * from ".DB_PREFIX."user where md5(code)='$code' and is_recover='1'");
	$query=$mysqli->store_result();
	$n_rows=$query->num_rows;
	if($n_rows!='1'){
    echo'<script>window.location="'.url.'";</script>';
  }else{
  $language=new language();
	$class_sitedata=new site();
?>
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-blue bg-lighten-2 blank-page blank-page">
      <div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
            <section class="flexbox-container">
              <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                  <div class="card-header no-border">
                    <div class="card-title text-xs-center">
                      <div class="p-1">
                        <h2 style="vertical-align: middle;"><img src="<?php echo res;?>icon/icon.png" style="width:45px;height45px;" alt="Puzzle Web"> <?php echo titlepage;?></h2>
                      </div>
                    </div>
                    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                      <span><?php echo $language->get_translate('recover_account');?></span>
                    </h6>
                  </div>
                  <div class="card-body">
                    <div class="card-block pt-0">
                      <form class="form-horizontal" id="submit">
                        <fieldset class="form-group floating-label-form-group">
                          <input type="hidden" name="code" value="<?php echo $code;?>">
                          <label for="user-name"><?php echo $language->get_translate('new_password');?> <font color="red">*</font></label>
                          <input type="password" class="form-control" id="password_1" name="password_1" placeholder="<?php echo $language->get_translate('new_password');?>" required="">
                        </fieldset>
                        <fieldset class="form-group floating-label-form-group">
                          <label for="user-name"><?php echo $language->get_translate('repeat_passoword');?> <font color="red">*</font></label>
                          <input type="password" class="form-control" id="password_2" name="password_2" placeholder="<?php echo $language->get_translate('repeat_passoword');?>" required="">
                        </fieldset>
                        <fieldset class="form-group row"></fieldset>
                        <button type="submit" class="btn btn-outline-warning btn-block"><i class="ft-unlock"></i> <?php echo $language->get_translate('recover_account');?></button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        $('#submit').on('submit',function(e){
          var block_ele = $(this).closest('.card-body');
          $(block_ele).block({
            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
            overlayCSS:{
              backgroundColor:'#fff',
              opacity:0.8,
              cursor:'wait',
            },
            css:{
              border:0,
              padding:'10px 15px',
              color:'#fff',
              width:'auto',
              backgroundColor:'#333',
            }
          });
          e.preventDefault();
          var form_data=new FormData(this);
          $.ajax({
            type:'POST',
            url:'<?php echo url;?>?action=reset',
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(form_data){
              if(form_data=='ok'){
                sweetAlert({title:"OK",text:'<?php echo $language->get_translate('ok_recover');?>',timer:2000,showConfirmButton:true,type:"success"},function(){setTimeout(go_to_login(),1000);});
                function go_to_login(){window.location="../";}
              }else{
                $(block_ele).unblock();
                sweetAlert({title:"Error",text:form_data,timer:1000,showConfirmButton:false,type:"error"});
                document.getElementById("password_1").value="";
                document.getElementById("password_2").value="";
              }
            }
          });
        });
      </script>
<?php }} ?>
