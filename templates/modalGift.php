<!-- Modal -->
<div class="modal fade" id="modalRegalos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Crear Regalo</h4>
      </div>
      <div class="modal-body">
		  <form class="form-horizontal" action="data/giftData.php" method="post" enctype="multipart/form-data">
  			<input type="hidden" name="pareja_id" value="<?php echo $weddingProfile['id_newperfil'];?>">

            <div class="form-group">
  			  <label class="col-md-4 control-label" for="aar">Tipo de regalos</label>
				<div class="col-md-6">
					<select  name="r_type" class="form-control">
						<option value="">Elige una opcion</option>
						<option value="">Sesion de Fotos en el Matrimonio</option>
						<option value="Video del matrimonio">Video del matrimonio</option>
						<option value="Remodelacion de vivienda">Remodelacion de vivienda</option>
						<option value="Redecoracion de dormitorio">Redecoracion de dormitorio</option>
						<option value="Nueva Cocina">Nueva Cocina</option>
						<option value="Botellas de Vino">Botellas de Vino</option>
						<option value="Televisor">Televisor</option>
						<option value="Clases de yoga">Clases de yoga</option>
						<option value="Vajilla Nueva">Vajilla Nueva </option>
						<option value="Regalo Personalizado">Regalo Personalizado</option>
					</select>
				</div>
  			</div>
            <div class="form-group">
				<label class="col-md-4 control-label" for="textinput">Imagen</label>
				<div class="col-md-8">
					<input type="file" name="img" class="form-control">
				</div>
			</div>
			<div class="form-group">
  			  <label class="col-md-4 control-label" for="aar">Descripcion</label>
				<div class="col-md-6">
					<textarea name="description" class="form-control"></textarea>
				</div>
  			</div>
			<div class="form-group">
  			  <label class="col-md-4 control-label" for="aar">Precio</label>
				<div class="col-md-6">
					<input type="number" name="amount" value="" class="form-control" style="width: 100%;">
				</div>
  			</div>
            <div class="form-group">
  			  <label class="col-md-4 control-label" for="aar">Abonos</label>
				<div class="col-md-6">
					<input type="number" name="payments" value="1" class="form-control" style="width: 100%;">
				</div>
  			</div>

  			<!-- Select Basic -->
  			<div class="form-group">
  			  <label class="col-md-4 control-label" for="send"></label>
  			  <div class="col-md-4">
  			    <button id="send"  class="btn btn-primary">Agregar</button>
  			  </div>
  			</div>

  		</form>
      </div>

    </div>
  </div>
</div>
