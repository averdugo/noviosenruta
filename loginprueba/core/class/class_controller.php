<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  if(!DEBUG){
		#No mostramos errores
		error_reporting(0);
	}

  if(HTTPS){
    # Forzamos https
    if($_SERVER["HTTPS"] != "on"){
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }
	}else{
    if(@$_SERVER["HTTPS"] == "on"){
        header("Location: http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }
  }
