<?php
	require_once '../libs/db.class.php';

	$imgName = $_FILES['img']['name'];
	$ext = pathinfo($imgName, PATHINFO_EXTENSION);
	$newName = time().'.'.$ext;
	$path =  '../img/uploads/'.$newName;

	if (move_uploaded_file($_FILES['img']['tmp_name'], $path)) {
		DB::insertUpdate('usuariosuser', array(
			'id' => $_POST['user_id']
		), 'image=%s', $newName);
		header("Location: ../userprofile.php");
		die();

	} else {
	    echo "¡Posible ataque de subida de ficheros!\n";
	}


?>
