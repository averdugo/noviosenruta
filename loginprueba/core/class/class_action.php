<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
	class action{
		//Creamos la variable de la ruta
		private $include;
		public function run($action){
			//Guardamos la ruta del fichero
			$this->include = 'core/app/action/'.$action.'-action.php';
			//Si el fichero existe, lo incluimos
			if(file_exists($this->include)){
				include_once $this->include;
			}else{
        header("HTTP/1.0 404 Not Found");
        include_once 'core/modules/404.php';
			}
		}
	}
	$action=new action();
	if(!isset($_GET['view']) && isset($_GET['action'])) {
		$action->run($_GET['action']);
	}
