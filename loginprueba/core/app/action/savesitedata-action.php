<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
      $url=$_POST['url'];
      $title=addslashes($_POST['title']);
      $description=addslashes($_POST['description']);
      $keywords=addslashes($_POST['keywords']);
      $site_mail=addslashes($_POST['site_mail']);
      $mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      mysqli_set_charset( $mysqli, 'utf8');
      $mysqli->query("update ".DB_PREFIX."config set var='$url' where slug='url'");
      $mysqli->query("update ".DB_PREFIX."config set var='$title' where slug='title'");
      $mysqli->query("update ".DB_PREFIX."config set var='$description' where slug='description'");
      $mysqli->query("update ".DB_PREFIX."config set var='$keywords' where slug='keywords'");
      $mysqli->query("update ".DB_PREFIX."config set var='$site_mail' where slug='site_mail'");
      if($mysqli){
        echo 'true';
      }else{
      	echo $language->get_translate('settings_update_error');
      }
    }
  }
