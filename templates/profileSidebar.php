<div class="col-md-3">
	<div class="profile-sidebar">
		<!-- SIDEBAR USERPIC -->
		<div class="profile-userpic">
			<?php
				if ($user['image']==NULL) { ?>
					<i class="glyphicon glyphicon-user goProfilePicture" style="font-size: 70px;cursor:pointer;"></i>
				<?php }else{ ?>
					<img src="img/uploads/<?php echo $user['image'] ;?>" alt="" class="goProfilePicture" style="cursor:pointer">
				<?php } ?>
				<div class="imgForProfile" style="display:none">
					<form class="" action="data/profileUpload.php" method="post" enctype="multipart/form-data">
						<input type="file" name="img" value="" id="imgForProfile" style="width: 100%;margin: 10px;">
						<input type="hidden" name="user_id" value="<?php echo $user['id'];?>">
						<button type="submit" class="btn btn-success" name="button">Subir Foto</button>
					</form>
				</div>
		</div>

		<div class="profile-usertitle">
			<div class="profile-usertitle-name">
				<?php echo $user['name'].' '.$user['surnames']; ?>
			</div>
		</div>

		<div class="profile-usermenu">
			<ul class="nav">
				<li class="active">
					<a href="userprofile.php" class="profileView">
					<i class="glyphicon glyphicon-home"></i>
					Perfil </a>
				</li>
				<li>
					<a href="userMiel.php" class="lunaView" >
					<i class="glyphicon glyphicon-ok"></i>
					Luna de Miel</a>
				</li>
				<li>
					<a href="userGiftList.php" >
					<i class="glyphicon glyphicon-flag"></i>
					Regalos</a>
				</li>
				<li>
					<a href="userAbono.php" >
					<i class="glyphicon glyphicon-usd"></i>
					Abonos</a>
				</li>
				<li>
					<div class="progress" style="width:80%;margin: 0 auto">
						<div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							<span class="">0% Complete</span>
						</div>
					</div>
				</li>

			</ul>
		</div>

	</div>
</div>
