<div id="formNoviosData"  class="sss">
	<form action="data/noviosData.php" method="POST" enctype="multipart/form-data" class="form-horizontal" style="margin-top:60px">
		<h2>Datos de los novios</h2>

		<?php if ($weddingProfile['id_newperfil']!=''): ?>
				<input type="hidden" name="weddingProfile_id" value="<?= $weddingProfile['id_newperfil'];?>">
		<?php endif; ?>

		<input type="hidden" name="user_id" value="<?php echo $user['id'];?>">
		<?php if ($weddingProfile['img'] != ""): ?>
			<img src="img/uploads/<?php echo $weddingProfile['img'];?>" alt="" class="img-responsive">
		<?php else: ?>
			<div class="form-group">
				<label class="col-md-4 control-label" for="textinput">Imagen Novios</label>
				<div class="col-md-8">
					<input type="file" name="img" class="form-control">
				</div>
			</div>
		<?php endif; ?>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Imagen de Fondo</label>
			<div class="col-md-8">
				<input type="file" name="imgFondo" class="form-control">
			</div>
		</div>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Nombre Novia</label>
			<div class="col-md-8">
				<input id="textinput" name="nombreNovia" type="text" placeholder="Nombre Novia" class="form-control input-md" value="<?php echo ( $weddingProfile['nom1_newperfil'] ) ? $weddingProfile['nom1_newperfil'] :''; ?>">
			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Nombre Novio</label>
			<div class="col-md-8">
				<input id="textinput" name="nombreNovio" type="text" placeholder="Nombre Novio" class="form-control input-md" required="" value="<?php echo ( $weddingProfile['nom2_newperfil'] ) ? $weddingProfile['nom2_newperfil'] :''; ?>">
			</div>
		</div>
<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Fecha de Boda</label>
			<div class="col-md-8">
				<input id="textinput" name="fechaboda" type="date" placeholder="" class="form-control input-md" value="<?php echo ( $weddingProfile['fechbod_newperfil'] )? $weddingProfile['fechbod_newperfil'] :''; ?>">
			</div>
		</div>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Telefono</label>
			<div class="col-md-8">
				<input id="textinput" name="phone" type="text" placeholder="+569xxxxx" class="form-control input-md" value="<?php echo ( $weddingProfile['phone'] )? $weddingProfile['phone'] :''; ?>">
			</div>
		</div>
		<!-- Prepended checkbox -->
		<h2>Datos de tu cuenta</h2>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Titular de la Cuenta</label>
			<div class="col-md-8">
				<input id="textinput" name="titular_cta" type="text" placeholder="" class="form-control input-md" required="required" value="<?php echo ( $weddingProfile['titular_cta'] )? $weddingProfile['titular_cta'] :''; ?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Mail de la Cuenta</label>
			<div class="col-md-8">
				<input id="textinput" name="mail_cta" type="email" placeholder="" class="form-control input-md" required="required" value="<?php echo ( $weddingProfile['mail_cta'] )? $weddingProfile['mail_cta'] :''; ?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Tipo de Cuenta</label>
			<div class="col-md-8">
				<select class="form-control" name="tipo_cta">
					<option value="">Seleccione</option>
					<option value="Cuenta Rut">Cuenta Rut</option>
					<option value="Cuenta Vista">Cuenta Vista</option>
					<option value="Cuenta Ahorra">Cuenta Ahorra</option>
					<option value="Cuenta Corriente">Cuenta Corriente</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Nombre del Banco</label>
			<div class="col-md-8">
				<input id="textinput" name="bank_name" type="text" placeholder="" class="form-control input-md" required="required" value="<?php echo ( $weddingProfile['bank_name'] )? $weddingProfile['bank_name'] :''; ?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Numero de Cuenta</label>
			<div class="col-md-8">
				<input id="textinput" name="bank_number" type="text" placeholder="" class="form-control input-md" required="required" value="<?php echo ( $weddingProfile['bank_number'] )? $weddingProfile['bank_number'] :''; ?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Saluda a tus invitados!</label>
			<div class="col-md-8">
				<textarea name="comments" id="comments" class="form-control" placeholder="Escribe un mensaje para tus invitados"><?php echo ( $weddingProfile['agrad_newperfil'] )? $weddingProfile['agrad_newperfil'] :''; ?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="btnsave">Guardar Cambios </label>
			<div class="col-md-8">
				<button type="submit" id="btnsave" name="btnsavtye" class="btn btn-success">si</button>
				<button id="btncancel" name="btncancel" class="btn btn-danger">No</button>
			</div>
		</div>
	</form>
	

</div>
