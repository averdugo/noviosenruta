<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'?destination=settings";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
	$id=$_SESSION['user_id'];
	$class_user=new user();
	$class_sitedata=new site();
  $data=new data();
?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
<?php echo $class_sitedata->sitedata('hook_before_body');?>
<?php include 'core/modules/nav-module.php';?>
<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
      <div class="row match-height">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="card-block">
                <div class="row">
                  <div class="col-md-12">
                    <form id="submit">
                      <textarea name="content" rows="30" class="form-control tinymceditor"><?php echo $data->selectdata('page','id','1','content');?></textarea>
                      <br>
                      <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('#submit').on('submit',function(e){
    e.preventDefault();
    var block_ele = $(this).closest('#submit');
    $(block_ele).block({
      message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
      overlayCSS:{
        backgroundColor:'#fff',
        opacity:0.8,
        cursor:'wait',
      },
      css:{
        border:0,
        padding:'10px 15px',
        color:'#fff',
        width:'auto',
        backgroundColor:'#333',
      }
    });
    var form_data=new FormData(this);
    $.ajax({
      type:'POST',
      url:'<?php echo url;?>?action=savepage',
      data:form_data,
      contentType: false,
      cache: false,
      processData:false,
      success:function(form_data){
        $(block_ele).unblock();
        if(form_data=='true'){
          sweetAlert("OK","<?php echo $language->get_translate('ok_settings');?>","success");
        }else{
          sweetAlert("Error",form_data,"error");
        }
      }
    });
  });
</script>
<?php }} ?>
