<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  define("VERSION","5.0",true);
  $version=VERSION;
  $request=file_get_contents('http://update.dacoto.com/update-admin-pro.json');
  $input=json_decode($request,true);
  $script=$input['Script'];
  $versionact=$input['Version'];
  $download=$input['Download'];
  echo 'Tienes instalado '.$script.' en su version '.$version.'.<br>';
  echo 'La ultima version publicada es la: '.$versionact.'.<br>';
  if($versionact==$version){
    echo 'Tienes la ultima version de '.$script.'.';
  }else{
    echo 'Hay una nueva version disponible.<br><br><a class="btn btn-warning" href="'.$download.'" target="_blank">Descargar Actualización</a>';
  }
?>
