<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
	class view{
		//Creamos la variable de la ruta
		private $include;
		public function run($view){
			//Guardamos la ruta del fichero
			$this->include = 'core/app/view/'.$view.'-view.php';
			//Si el fichero existe, lo incluimos
			if(file_exists($this->include)){
				require_once($this->include);
			}else{
        header("HTTP/1.0 404 Not Found");
        include_once 'core/modules/404.php';
			}
		}
	}
	$view=new view();
	if (!isset($_GET['view']) && !isset($_GET['action'])) {
		$view->run('index');
	}else if(isset($_GET['view']) && !isset($_GET['action'])) {
		$view->run($_GET['view']);
	}else if(isset($_GET['view']) && isset($_GET['action'])) {
    header("HTTP/1.0 403 Permision Denied");
		echo '<br><b>403 PERMISION DENIED</b> These sent two variables that can cause error! - <a target="_blank" href="https://www.dacoto.com/forums/forum/puzzle-web/">Help</a>';
	}
