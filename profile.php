<?php include  "templates/layout.php"; ?>
<?php

	$weddingProfile = DB::queryFirstRow("SELECT * FROM pareja_newperfil WHERE id_newperfil=%s", $_GET["id"]);
	$honeyMoon= DB::queryFirstRow("SELECT * FROM honey_moons WHERE pareja_id=%s", $weddingProfile['id_newperfil']);
	$gifts = DB::query("SELECT * FROM regalos WHERE pareja_id=%s", $weddingProfile['id_newperfil']);


 ?>
 <style media="screen">
	  .cardGift{
		  width: 70%;
		  margin: 10px auto;
		  background-color: rgba(255, 255, 255, 0.6);
		  display: block;
		  padding: 10px;
		  height: 150px;
		  box-shadow: 1px 2px 4px 0 rgba(0, 0, 0, 0.08);
		  border: 1px solid #dbdbdb;
		  text-align: left;
		  text-shadow: none;
		  color:black
	  }
	  .media-right{
		  width: 25%;
		  text-align: right
	  }
	  .bannerFooter{
		  font-size: 19px;
	      background: #FFFFFF none repeat scroll 0% 0%;
	      height: 100px;
	      position: fixed;
	      bottom: -100px;
	      width: 100%;
	      z-index: 2000;
	      border-top: 2px none #FFF;
	      box-shadow: 0px -5px 5px rgba(0, 0, 0, 0.28);
	      opacity: 1;
	      left: 0;
	      -webkit-transition-property: top, bottom;
	      -webkit-transition-duration: 0.5s;
		  padding: 30px;
	      -webkit-overflow-scrolling: touch;
	  }
	  .contBanner{
		  margin: 0px auto;
      	width: 600px;
	  }
	  .bannerFooter span{
	  	font-size:20px;
		color:black
	  }
  </style>
<body id="body" data-spy="scroll" data-target=".header">
	<?php include $header ?>
	<style media="screen">
		td{
			text-align: left;
		}
	</style>
	<div class="carousel slide carousel-fade" data-ride="carousel">

	<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<?php if ($weddingProfile['img_fondo'] != ""): ?>
				<div class="item active" style="background: url(img/uploads/<?php echo $weddingProfile['img_fondo']; ?>) no-repeat center center fixed;    background-size: cover;">
			<?php else: ?>
				<div class="item active" style="background: url(http://elisabethtours.com/wp-content/uploads/2017/04/Colonia-Alemania-carnaval-t.jpg) no-repeat center center fixed;    background-size: cover;">
			<?php endif; ?>

			</div>

		</div>
	</div>
	<div id="page">

		<div id="products">
            <div class="container content-lg">

                <div class="row text-center margin-b-40">
					<div class="title" style="background-color:rgba(255,255,255,0.8)">
						<div class="responsive-blog-post">

							<div class="individual-post">
								<h4 style="color: white"><?php echo $weddingProfile['nom1_newperfil']." y ".$weddingProfile['nom2_newperfil'];  ?></h4>

								<p class="date"><?php echo $weddingProfile['fechbod_newperfil']; ?></p>
								<img src="img/uploads/<?php echo $weddingProfile['img']; ?>" class="img-responsive">
								<h1 style="color: white" class="line-title">Agradecimientos</h1>
								<p style="color:white"><?php echo $weddingProfile['agrad_newperfil']; ?></p>
							</div>

							<div class="" style="padding:20px">
								<h1 style="color: white" class="line-title">Luna de Miel</h1>
								<div class="media cardGift">
								  <div class="media-body">
									<h4 class="media-heading"><?php echo $honeyMoon['plan']; ?></h4>
									<p><strong>Donde:</strong> <?php echo $honeyMoon['where']; ?> <strong>Dias:</strong> <?php echo $honeyMoon['days']; ?></p>
									<p><?php echo $honeyMoon['description']; ?></p>

								  </div>
								  <div class="media-right">
									<h4>
										$ <?php
												$unit = $honeyMoon['amount'] / $honeyMoon['payments'];
												echo $unit;
											?>
									</h4>
									<h5>Faltan <?php echo $honeyMoon['payments']; ?> Abonos</h5>
									<div class="">
										<!-- Single button -->
										<select class="form-control selectGifts" name="">
											<option value="">Regalanos</option>
											<?php
												for ($i=1; $i <= $honeyMoon['payments']  ; $i++) { ?>
													<option value="<?php echo $i ?>"><?php echo $i ?></option>
											<?php } ?>
										</select>
									  </div><!-- /.col-lg-6 -->
								  </div>
								</div>
							</div>

							<div class="" style="padding:20px;margin:0 auto">
								<h1 style="color: white" class="line-title">Lista de Regalos</h1>
										<?php foreach ($gifts as $key => $value): ?>
											<div class="media cardGift">
											  <div class="media-left">
											    <a href="#">
													<img src="img/uploads/<?php echo $value['img']; ?>" class="media-object img-responsive" alt="" style="max-width:150px;margin:0">
											    </a>
											  </div>
											  <div class="media-body">
											    <h4 class="media-heading"><?php echo $value['name']; ?></h4>
											    <p><?php echo $value['description']; ?></p>
											  </div>
											  <div class="media-right">
											  	<h4>$ <?php
														$unit = $value['amount'] / $value['payments'];
														echo $unit;
													?>
												</h4>
												<h5>Faltan <?php echo $value['payments']; ?> Abonos</h5>
												<div class="">
													<!-- Single button -->
													<select class="form-control selectGifts" name="" data-pay="<?php echo $unit; ?>">
														<option value="">Regalanos</option>
														<?php
															for ($i=1; $i <= $value['payments']  ; $i++) { ?>
																<option value="<?php echo $i ?>" ><?php echo $i ?></option>
														<?php } ?>
													</select>
												  </div><!-- /.col-lg-6 -->
											  </div>
											</div>
										<?php endforeach; ?>

										<div class="" style="padding:20px;margin:0 auto">
											<h1 style="color: white" class="line-title">Mensajes</h1>
										</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include 'templates/footer.php'; ?>
<div class="bannerFooter">
	<div class="contBanner">
		<div class="col-md-4">
			<i class="glyphicon glyphicon-gift" style="font-size:34px"></i><span id="selectItems"></span>Items
		</div>
		<div class="col-md-4">
			<span style="font-size:34px">$&nbsp;&nbsp;</span> <span id="totalPat"></span>Total
		</div>
		<div class="col-md-4">
			<button type="button" class="btn btn-lg btn-block btn-success" name="button">Continuar</button>
		</div>
	</div>
</div>
</body>
</html>
