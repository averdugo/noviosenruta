<?php
	include  "templates/layout.php";
	session_start();
	$user = DB::queryFirstRow("SELECT * FROM usuariosuser WHERE id=%s", $_SESSION['user_id']);
	$weddingProfile = DB::queryFirstRow("SELECT * FROM pareja_newperfil WHERE user_id=%s", $_SESSION['user_id']);


?>
<link rel="stylesheet" href="css/multiple-select.css">

<body id="body" data-spy="scroll" data-target=".header">
	<?php include $header ?>

	<style media="screen">
		.sss{
			width: 80%;
			margin: 0 auto;
		}
		textarea.form-control{
			border: 1px solid;
			height: 100px
		}

	</style>

	<div id="page">
		<div class="promo-block">
			<div class="container">
				<div class="row profile">
					<?php include "templates/profileSidebar.php"; ?>
					<div class="col-md-9">
						<div class="profile-content">
							<?php include "templates/formLunaMiel.php"; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include $footer ?>
	<script src="js/multiple-select.js"></script>
	<script type="text/javascript">
		$('.countrySelect').multipleSelect();
	</script>


</body>
</html>
