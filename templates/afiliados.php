<div class="container">
	<h3 style="color: black" align="float:left">Nuestros Afiliados</h3>
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
		<div class="btn-group" role="group">
		 <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-music" aria-hidden="true">
			 <div class="hidden-xs">Dj y Musica</div>
		 </button>
		</div>
		<div class="btn-group" role="group">
		 <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicons-food" aria-hidden="true"></span>
			 <div class="hidden-xs">Banqueteria</div>
		 </button>
		</div>
		<div class="btn-group" role="group">
		 <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
			 <div class="hidden-xs">Fotografia y Video</div>
		 </button>
		</div>
		<div class="btn-group" role="group">
		 <button type="button" id="following" class="btn btn-default" href="#tab4" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			 <div class="hidden-xs">Planeadores de Bodas</div>
		 </button>
		</div>
		<div class="btn-group" role="group">
		 <button type="button" id="following" class="btn btn-default" href="#tab5" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			 <div class="hidden-xs">Peluqueria & Spa</div>
		 </button>
		</div>
	</div>
	<div class="well">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1">
				<!---foto-->
				<!---foto-->
				<h3>Banda La Gran Boogie</h3>
				<img src="img/boogie.png">
				<p>Contacto:</p><a href="mailto:"> jaimelopez@lagranboogie.cl</a>
				<a href="mailto:"> </a>
				<p>Al contratar pack banda + Amplificación/Iluminacion, te llevas de regalo el DJ y el cañón turbina de confeti.</p>
				<p>Telefono:</p><a href="tel:">  +569 88340025</a>
				<a href="">www.lagranboogie.cl</a>
				<!--foto-->
				<!---foto-->
				<h3>Canto Lirico</h3>
				<img width="100%" height="auto" src="img/canto.jpg">
				<p>Contacto:</p><a href="mailto:"> jennymus@gmail.com</a>
				<a href="mailto:"> </a>
				<p>10% (diez porciento) de descuento en servicio de coro y música, celebración de matrimonio.</p>
				<p>15% (quince porciento) de descuento en servicio coro y música, celebración de matrimonio y recepción.</p>
				<p>20% (veinte porciento) de descuento en servicio coro y música, celebración de matrimonio y cena.</p>
				<p>Telefono:</p><a href="tel:">   +569 82134246</a>
				<a href="">www.cantolirico.cl</a>
			<!--foto-->
			</div>
			<div class="tab-pane fade in" id="tab2">
				<h3>Pablo Pinto Banquetería</h3>
				<img width="200" height="200" src="img/pablo.jpg">
				<p>Contacto:</p><a href="mailto:">  pablo@pablopinto.cl</a>
				<a href="mailto:"> </a>
				<p>Telefono:</p><a href="tel:">   +56 2 22405052 </a>
				<p>5% de descuento en el total de los servicios.
				</p>
				<a href="tel:">+56 2 22405017</a>

				<a href="">www.pablopinto.cl</a>
			</div>
			<div class="tab-pane fade in" id="tab3">
				<h3>Wap Films</h3>
				<img src="img/wap1.png">
				<p>Contacto:</p><a href="mailto:"> wafer@wapfilms.com</a>
				<a href="mailto:">laura@wapfilms.com</a>
				<p>Telefono:</p><a href="tel:"> +569 66595481</a>
				<p>8% de descuento en el total de los servicios.</p>
				<a href="">www.wapfilms.com</a>
			</div>
			<div class="tab-pane fade in" id="tab4">
				<h3>Twins Planners</h3>
				<img width="100%" height="auto" src="img/twin.png">
				<p>Contacto:</p><a href="mailto:"> karin@twinsplanners.cl</a>
				<a href="mailto:"></a>
				<p>Telefono:</p><a href="tel:"> +56 999981958</a>
				<p>7% dcto. en planes para novios (super full, 50-50, día del matrimonio y novias en el extranjero).</p>
				<a href="">www.twinsplanners.cl</a>
				<!--marcador-->
				<h3>Nectar Ideas</h3>
				<img src="img/nectar.jpg">
				<p>Contacto:</p><a href="mailto:"> contacto@nectarideas.cl</a>
				<a href="mailto:"></a>
				<p>Telefono:</p><a href="tel:"> +56 9 93644777</a>
				<p>10% (diez por ciento) de descuento comprando solo Partes.</p>
				<p>15% (quince por ciento) de descuento comprando Partes y Recuerdos.</p>
				<a href="">www.twinsplanners.cl</a>
			</div>
			<div class="tab-pane fade in" id="tab5">
				<h3>Magnifique Spa & Boutique</h3>
				<img  src="img/spa.png">
				<p>Contacto:</p><a href="mailto:">   contacto@magnifiquespa.cl</a>
				<a href="mailto:"> </a>
				<p>Telefono:</p><a href="tel:">    +562 32220157</a>
				<p>•15% dcto. para la novia y Madrina, en Servicios de Spa de
				Manicure, Pedicure (Tradicional, Permanente) y Peinados.
				Regalos para la Novia: Cristales Swarovski en 2 uñas y
				Parafinoterapia (hidratación profunda en Manos).</p>
				<p>•    15% dcto. para todas las invitadas al matrimonio en todos los
				Servicios
				</p>
				<p>•    10% dcto. en nuestra exclusiva Boutique de joyas con
				Cristales Swarovski y Carteras.</p>
				<a href="tel:"></a>
				<a href="">www.magnifiquespa.cl</a>
			</div>
		</div>
	</div>
</div>
