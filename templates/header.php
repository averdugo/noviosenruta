<header class="header navbar-fixed-top">
	<!-- Navbar -->
	<nav class="navbar" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="menu-container js_nav-item">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="toggle-icon"></span>
				</button>

				<!-- Logo -->
				<div class="logo">
					<a class="logo-wrap" href="#body">
						<img class="logo-img logo-img-main" src="img/logo.png" width="500" height="500" alt="Novios en Ruta Logo Logo">
						<img class="logo-img logo-img-active" src="img/logo-dark.png" width="500" height="500" alt="Novios en Ruta logo">
					</a>
				</div>
				<!-- End Logo -->
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse nav-collapse">
				<div class="menu-container">
					<ul class="nav navbar-nav navbar-nav-right">
						<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="index.php">Home</a></li>
						<li class="js_nav-item nav-item dropdown">
							<a href="#" class="dropdown-toggle nav-item-child nav-item-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Novios <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="sobre.php">¿Como funciona?</a></li>
								<li><a href="loginprueba/auth/register">Registrate</a></li>
						  	</ul>
						</li>
						<li class="js_nav-item nav-item dropdown">
							<a href="#" class="dropdown-toggle nav-item-child nav-item-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Invitados <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#" data-toggle="modal" data-target="#myModal">Regala</a></li>

						  	</ul>
						</li>
						<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="contacto.php">Contacto</a></li>
						<label><span>&nbsp;</span><input type="submit" value="Ingresa" onClick="window.location.href='/loginprueba/index.php'" /></label>
					</ul>
				</div>
			</div>
			<!-- End Navbar Collapse -->
		</div>
	</nav>
	<!-- Navbar -->
</header>
