<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'?destination=users/home";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
  $language=new language();
	$id=$_SESSION['user_id'];
	$class_user=new user();
	$class_sitedata=new site();
?>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
  <?php echo $class_sitedata->sitedata('hook_before_body');?>
  <?php include 'core/modules/nav-module.php';?>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row"></div>
      <div class="content-body">
        <div class="row match-height">
      		<div class="col-md-12">
            <div class="card">
      				<div class="card-header">
      					<h4 class="card-title anti_select" id="basic-layout-form-center"><?php echo $language->get_translate('check_update');?></h4>
      					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      					<div class="heading-elements">
      						<ul class="list-inline mb-0">
      							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
      						</ul>
      					</div>
      				</div>
              <div class="card-body checkupdate">
                <div class="card-block">
                  <button class="btn btn-success" id="checkupdate"><i class="ft-refresh-ccw"></i> <?php echo $language->get_translate('check_update');?></button><br><br>
    							<div id="update"></div>
                </div>
              </div>
            </div>
            <div class="card">
      				<div class="card-header">
      					<h4 class="card-title anti_select" id="basic-layout-form-center"> <?php echo $language->get_translate('to_update');?></h4>
      					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      					<div class="heading-elements">
      						<ul class="list-inline mb-0">
      							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
      						</ul>
      					</div>
      				</div>
              <div class="card-body">
                <div class="card-block">
                  <form id="loading" method="post" action="<?php echo url;?>?action=update" enctype="multipart/form-data">
                    <label><?php echo $language->get_translate('update_info');?></label>
                    <input type="file" name="zip" required="" class="form-control" accept=".zip">
                    <br>
                    <button class="btn btn-success" type="submint"><i class="ft-upload"></i> <?php echo $language->get_translate('update');?></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $("#loading").submit(function(){
      var block_ele = $(this).closest('.card-body');
      $(block_ele).block({
        message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
        overlayCSS:{
          backgroundColor:'#fff',
          opacity:0.8,
          cursor:'wait',
        },
        css:{
          border:0,
          padding:'10px 15px',
          color:'#fff',
          width:'auto',
          backgroundColor:'#333',
        }
      });
    });
    $("#checkupdate").click(function(){
      var block_ele = $(this).closest('.checkupdate');
      $(block_ele).block({
        message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
        overlayCSS:{
          backgroundColor:'#fff',
          opacity:0.8,
          cursor:'wait',
        },
        css:{
          border:0,
          padding:'10px 15px',
          color:'#fff',
          width:'auto',
          backgroundColor:'#333',
        }
      });
      $.get("<?php echo url;?>check_update.php",function(data){
        document.getElementById("update").innerHTML = data;
          $(block_ele).unblock();
      });
    });
  </script>
<?php }} ?>
