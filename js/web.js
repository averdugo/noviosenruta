$(function(){

	$.get("data/parejasName.php",function(r){
		var data = JSON.parse(r);
		var optionsAutocomplete = {
			data: data,
			getValue: "character",
			list: {
				maxNumberOfElements: 10,
				match: {
					enabled: true
				},
				onClickEvent: function() {
					var value = $("#parejaSearch").getSelectedItemData().id;
					location.href="profile.php?id="+value
				}
			}
		};

		$("#parejaSearch").easyAutocomplete(optionsAutocomplete);
	})


	$('body').on('click','.goProfilePicture',function(){
		$('#imgForProfile').click()
		$('.imgForProfile').fadeIn()
	})

	$('.profileView').click(function(){
		$('.sss').hide();
		$('#formNoviosData').fadeIn()
	})
	$('.lunaView').click(function(){

		$('.sss').hide()
		$('#formLunaMiel').fadeIn();
	})

	$('#aar').on('change',function(){
		$('.hideSelects').hide();
		if (this.value == 5 || this.value == 6 || this.value == 7 || this.value == 8) {
			var where = "";
			switch(this.value) {
				    case '5':
				        where ="América central y México";
				        break;
				    case '6':
				        where ="Caribe";
				        break;
					case '7':
					    where ="África";
					    break;
					case '8':
				        where ="Oceanía";
				        break;
				}
			$('#whereHide').val(where)
		}else{
			$('#reg'+this.value).fadeIn();
		}


	})

	$('.whereR').on('change',function(){
		$('#whereHide').val(this.value)
	})
	var items = 0;
	var total = 0;
	$('.selectGifts').on('change',function(){
		var value = 0;
		items = items + parseInt(this.value);
		value = parseInt($(this).data('pay')) * parseInt(this.value);
		total = total + value;
		if (items > 0) {
			$('#selectItems').html('&nbsp;&nbsp;&nbsp;&nbsp;'+items)
			$('#totalPat').html(total+' ')
			$('.bannerFooter').css('bottom', '0px')

		}
	})


})
