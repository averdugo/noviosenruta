<?php
  if(isset($_SESSION['user_mail'])){echo'<script>window.location="'.url.'/../../../../userprofile.php";</script>';}else{
  $language=new language();
  $class_sitedata=new site();
?>
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-blue backnuev bg-lighten-2 blank-page blank-page" background="matri.jpg">
      <div class="app-content content container-fluid">
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
            <section class="flexbox-container">
              <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                  <div class="card-header no-border">
                    <div class="card-title text-xs-center">
                      <div class="p-1">
                        <h2 style="vertical-align: middle;"><img src="<?php echo res;?>icon/icon.png" style="width:45px;height45px;" alt="Puzzle Web"> <?php echo titlepage;?></h2>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="card-block pt-0">
                      <form class="form-horizontal" id="submit">
                        <input type="hidden" name="destination" id="destination" value="<?php echo @$_GET['destination'];?>">
                        <fieldset class="form-group floating-label-form-group">
                          <label for="user-name"><?php echo $language->get_translate('email');?></label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $language->get_translate('email');?>" required="">
                        </fieldset>
                        <fieldset class="form-group floating-label-form-group mb-1">
                          <label for="user-password"><?php echo $language->get_translate('password');?></label>
                          <input type="password" class="form-control" id="password" name="password" placeholder="<?php echo $language->get_translate('password');?>" required="">
                        </fieldset>
                        <fieldset class="form-group row">
                          <div class="col-md-6 col-xs-12 text-xs-center text-sm-left"></div>
                          <div class="col-md-6 col-xs-12 float-sm-left text-xs-center text-sm-right blue"><a href="<?php echo url;?>auth/recover" class="card-link"><?php echo $language->get_translate('recover_answer');?></a></div>
                        </fieldset>
                        <button type="submit" class="btn btn-outline-blue btn-block"><i class="ft-unlock"></i> <?php echo $language->get_translate('login');?></button>
                        <a href="<?php echo url;?>auth/active" class="btn btn-outline-warning btn-block"><i class="fa fa-toggle-off"></i> <?php echo $language->get_translate('active_button');?></a>
                      </form>
                    </div>
                    <?php if($class_sitedata->sitedata('allow_registers')=='1'){ ?>
                    <p class="card-subtitle line-on-side text-muted text-xs-center font-small-3 mx-2 my-1">
                      <span><?php echo $language->get_translate('register_answer');?></span>
                    </p>
                    <div class="card-block">
                      <a href="<?php echo url;?>auth/register" class="btn btn-outline-danger btn-block"><i class="ft-user"></i> <?php echo $language->get_translate('register_button');?></a>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        $('#submit').on('submit',function(e){
          var block_ele = $(this).closest('.card-body');
          $(block_ele).block({
            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
            overlayCSS:{
              backgroundColor:'#fff',
              opacity:0.8,
              cursor:'wait',
            },
            css:{
              border:0,
              padding:'10px 15px',
              color:'#fff',
              width:'auto',
              backgroundColor:'#333',
            }
          });
          e.preventDefault();
          var form_data=new FormData(this);
          $.ajax({
            type:'POST',
            url:'<?php echo url;?>?action=login',
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(form_data){
              if(form_data=='ok'){
                if($("#destination").val()!=''){
                  window.location=$("#destination").val();
                }else{
                  window.location="/userprofile.html";
                }
              }else{
                $(block_ele).unblock();
                sweetAlert({title:"Error",text:form_data,timer:1000,showConfirmButton:false,type:"error"});
                document.getElementById("password").value="";
              }
            }
          });
        });
      </script>
<?php } ?>
