<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'?destination=settings";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
  $language=new language();
	$id=$_SESSION['user_id'];
	$class_user=new user();
	$class_sitedata=new site();
?>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
  <?php echo $class_sitedata->sitedata('hook_before_body');?>
  <?php include 'core/modules/nav-module.php';?>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row"></div>
      <div class="content-body">
        <div class="row match-height">
      		<div class="col-md-12">
      			<div class="card">
      				<div class="card-header">
      					<h4 class="card-title anti_select" id="basic-layout-form-center"><?php echo $language->get_translate('settings');?></h4>
      					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      					<div class="heading-elements">
      						<ul class="list-inline mb-0">
      							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
      						</ul>
      					</div>
      				</div>
              <div class="card-body">
                <div class="card-block">
      						<ul class="nav nav-tabs nav-justified">
      							<li class="nav-item"><a class="nav-link active" id="tab_1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true"><?php echo $language->get_translate('settings_edit');?></a></li>
      							<li class="nav-item"><a class="nav-link" id="tab_2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false"><?php echo $language->get_translate('settings_cog');?></a></li>
      							<li class="nav-item"><a class="nav-link" id="tab_3" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false"><?php echo $language->get_translate('settings_email');?></a></li>
      							<li class="nav-item"><a class="nav-link" id="tab_4" data-toggle="tab" aria-controls="tab4" href="#tab4" aria-expanded="false"><?php echo $language->get_translate('settings_hooks');?></a></li>
      						</ul>
      						<div class="tab-content px-1 pt-1">
      							<div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="tab_1">
                      <form class="form form-horizontal" id="submit_1">
                        <div class="form-body">
                          <h4 class="form-section"><i class="ft-globe"></i> <?php echo $language->get_translate('site_data');?></h4>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('site_url');?></label>
                            <div class="col-md-9">
                              <input type="url" class="form-control" placeholder="<?php echo $language->get_translate('site_url');?>" name="url" required="" value="<?php echo $class_sitedata->sitedata('url');?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('title');?></label>
                            <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="<?php echo $language->get_translate('title');?>" name="title" id="title" required="" value="<?php echo $class_sitedata->sitedata('title');?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('description');?></label>
                            <div class="col-md-9">
                              <textarea rows="4" class="form-control" name="description" placeholder="<?php echo $language->get_translate('description');?>" required=""><?php echo $class_sitedata->sitedata('description');?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('keywords');?></label>
                            <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="<?php echo $language->get_translate('keywords');?>" name="keywords" required="" value="<?php echo $class_sitedata->sitedata('keywords');?>">
                            </div>
                          </div>
                          <h4 class="form-section"><i class="ft-user"></i> <?php echo $language->get_translate('admin_data');?></h4>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('admin_email');?></label>
                            <div class="col-md-9">
                              <input type="email" class="form-control" placeholder="<?php echo $language->get_translate('admin_email');?>" name="site_mail" required="" value="<?php echo $class_sitedata->sitedata('site_mail');?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-actions">
                          <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $language->get_translate('save');?></button>
                        </div>
                      </form>
                      <script>
                        $('#submit_1').on('submit',function(e){
                          e.preventDefault();
                          var block_ele = $(this).closest('#submit_1');
                          $(block_ele).block({
                            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
                            overlayCSS:{
                              backgroundColor:'#fff',
                              opacity:0.8,
                              cursor:'wait',
                            },
                            css:{
                              border:0,
                              padding:'10px 15px',
                              color:'#fff',
                              width:'auto',
                              backgroundColor:'#333',
                            }
                          });
                          var form_data=new FormData(this);
                          $.ajax({
                            type:'POST',
                            url:'<?php echo url;?>?action=savesitedata',
                            data:form_data,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success:function(form_data){
                              $(block_ele).unblock();
                              if(form_data=='true'){
                                $('#html_site_title').html($("#title").val());
                                $('#site_title').html($("#title").val());
                                sweetAlert("OK","<?php echo $language->get_translate('ok_settings');?>","success");
                              }else{
                                sweetAlert("Error",form_data,"error");
                              }
                            }
                          });
                        });
                      </script>
      							</div>
      							<div class="tab-pane" id="tab2" aria-labelledby="tab_2" aria-expanded="false">
                      <form class="form form-horizontal" id="submit_2">
                        <div class="form-body">
                          <h4 class="form-section"><i class="ft-user"></i> <?php echo $language->get_translate('general_cogs');?></h4>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('users_registers');?></label>
                            <div class="col-md-9">
                              <fieldset>
                                <label class="custom-control custom-radio">
                                  <input value="1" name="allow_registers" type="radio" class="custom-control-input" <?php if($class_sitedata->sitedata('allow_registers')=='1'){echo ' checked=""';};?>>
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description"><?php echo $language->get_translate('yes');?></span>
                                </label>
                              </fieldset>
                              <fieldset>
                                <label class="custom-control custom-radio">
                                  <input value="0" name="allow_registers" type="radio" class="custom-control-input" <?php if($class_sitedata->sitedata('allow_registers')=='0'){echo ' checked=""';};?>>
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description"><?php echo $language->get_translate('no');?></span>
                                </label>
                              </fieldset>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('default_language');?></label>
                            <div class="col-md-9">
                              <select class="form-control" name="language" required="">
                                <option value="" disabled=""><?php echo $language->get_translate('select_language');?></option>
                                <?php
                                  $language=new language();
                                  echo $language->list_language_settings();
                                ?>
                              </select>
                            </div>
                          </div>
                          <h4 class="form-section"><i class="fa fa-google-plus"></i> <?php echo $language->get_translate('google_plus_in_use');?></h4>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('active_google_plus');?></label>
                            <div class="col-md-9">
                              <fieldset>
                                <label class="custom-control custom-radio">
                                  <input value="1" name="google_active" type="radio" class="custom-control-input" <?php if($class_sitedata->sitedata('google_active')=='1'){echo ' checked=""';};?>>
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description"><?php echo $language->get_translate('yes');?></span>
                                </label>
                              </fieldset>
                              <fieldset>
                                <label class="custom-control custom-radio">
                                  <input value="0" name="google_active" type="radio" class="custom-control-input" <?php if($class_sitedata->sitedata('google_active')=='0'){echo ' checked=""';};?>>
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description"><?php echo $language->get_translate('no');?></span>
                                </label>
                              </fieldset>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control">Google client_id</label>
                            <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Google client_id" name="google_client_id" value="<?php echo $class_sitedata->sitedata('google_client_id');?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control">Google client_secret</label>
                            <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Google client_secret" name="google_client_secret" value="<?php echo $class_sitedata->sitedata('google_client_secret');?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-actions">
                          <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $language->get_translate('save');?></button>
                        </div>
                      </form>
                      <script>
                        $('#submit_2').on('submit',function(e){
                          e.preventDefault();
                          var block_ele = $(this).closest('#submit_2');
                          $(block_ele).block({
                            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
                            overlayCSS:{
                              backgroundColor:'#fff',
                              opacity:0.8,
                              cursor:'wait',
                            },
                            css:{
                              border:0,
                              padding:'10px 15px',
                              color:'#fff',
                              width:'auto',
                              backgroundColor:'#333',
                            }
                          });
                          var form_data=new FormData(this);
                          $.ajax({
                            type:'POST',
                            url:'<?php echo url;?>?action=savesettings',
                            data:form_data,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success:function(form_data){
                              $(block_ele).unblock();
                              if(form_data=='true'){
                                sweetAlert("OK","<?php echo $language->get_translate('ok_settings');?>","success");
                              }else{
                                sweetAlert("Error",form_data,"error");
                              }
                            }
                          });
                        });
                      </script>
                    </div>
      							<div class="tab-pane" id="tab3" aria-labelledby="tab_3" aria-expanded="false">
                      <form class="form form-horizontal" id="submit_3">
                        <div class="form-body">
                          <h4 class="form-section"><i class="ft-mail"></i> <?php echo $language->get_translate('settings_email');?></h4>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('email_register');?></label>
                            <div class="col-md-9">
                              <textarea rows="12" class="form-control" name="email_register" required=""><?php echo $class_sitedata->sitedata('email_register');?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('email_recover');?></label>
                            <div class="col-md-9">
                              <textarea rows="12" class="form-control" name="email_recover" required=""><?php echo $class_sitedata->sitedata('email_recover');?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control"><?php echo $language->get_translate('email_active');?></label>
                            <div class="col-md-9">
                              <textarea rows="12" class="form-control" name="email_active" required=""><?php echo $class_sitedata->sitedata('email_active');?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="form-actions">
                          <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $language->get_translate('save');?></button>
                        </div>
                      </form>
                      <script>
                        $('#submit_3').on('submit',function(e){
                          e.preventDefault();
                          var block_ele = $(this).closest('#submit_3');
                          $(block_ele).block({
                            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
                            overlayCSS:{
                              backgroundColor:'#fff',
                              opacity:0.8,
                              cursor:'wait',
                            },
                            css:{
                              border:0,
                              padding:'10px 15px',
                              color:'#fff',
                              width:'auto',
                              backgroundColor:'#333',
                            }
                          });
                          var form_data=new FormData(this);
                          $.ajax({
                            type:'POST',
                            url:'<?php echo url;?>?action=saveemail',
                            data:form_data,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success:function(form_data){
                              $(block_ele).unblock();
                              if(form_data=='true'){
                                sweetAlert("OK","<?php echo $language->get_translate('ok_settings');?>","success");
                              }else{
                                sweetAlert("Error",form_data,"error");
                              }
                            }
                          });
                        });
                      </script>
                    </div>
      							<div class="tab-pane" id="tab4" aria-labelledby="tab_4" aria-expanded="false">
                      <form class="form form-horizontal" id="submit_4">
                        <div class="form-body">
                          <h4 class="form-section"><i class="fa fa-code"></i> <?php echo $language->get_translate('settings_hooks');?></h4>
                          <div class="form-group row">
                            <label class="col-md-3 label-control">hook_before_header</label>
                            <div class="col-md-9">
                              <textarea rows="10" class="form-control" name="hook_before_header"><?php echo $class_sitedata->sitedata('hook_before_header');?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control">hook_after_header</label>
                            <div class="col-md-9">
                              <textarea rows="10" class="form-control" name="hook_after_header"><?php echo $class_sitedata->sitedata('hook_after_header');?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control">hook_before_body</label>
                            <div class="col-md-9">
                              <textarea rows="10" class="form-control" name="hook_before_body"><?php echo $class_sitedata->sitedata('hook_before_body');?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-3 label-control">hook_after_body</label>
                            <div class="col-md-9">
                              <textarea rows="10" class="form-control" name="hook_after_body"><?php echo $class_sitedata->sitedata('hook_after_body');?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="form-actions">
                          <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $language->get_translate('save');?></button>
                        </div>
                      </form>
                      <script>
                        $('#submit_4').on('submit',function(e){
                          e.preventDefault();
                          var block_ele = $(this).closest('#submit_4');
                          $(block_ele).block({
                            message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
                            overlayCSS:{
                              backgroundColor:'#fff',
                              opacity:0.8,
                              cursor:'wait',
                            },
                            css:{
                              border:0,
                              padding:'10px 15px',
                              color:'#fff',
                              width:'auto',
                              backgroundColor:'#333',
                            }
                          });
                          var form_data=new FormData(this);
                          $.ajax({
                            type:'POST',
                            url:'<?php echo url;?>?action=savehooks',
                            data:form_data,
                            contentType: false,
                            cache: false,
                            processData:false,
                            success:function(form_data){
                              $(block_ele).unblock();
                              if(form_data=='true'){
                                sweetAlert("OK","<?php echo $language->get_translate('ok_settings');?>","success");
                              }else{
                                sweetAlert("Error",form_data,"error");
                              }
                            }
                          });
                        });
                      </script>
                    </div>
      						</div>
      					</div>
              </div>
          	</div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }} ?>
