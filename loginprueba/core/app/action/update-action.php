<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
    if($_SESSION['user_role']!='1'){echo'<script>window.location="'.url.'dashboard";</script>';}else{
      $language=new language();
      $upload_path='data/updates/';
      $file=$upload_path.basename($_FILES['zip']['name']);
      if($_FILES['zip']['name']=='last_update_admin_pro.zip'){
        if (move_uploaded_file($_FILES['zip']['tmp_name'], $file)){
          move_uploaded_file($_FILES['zip']['tmp_name'], $file);
          $enzipado = new ZipArchive();
          $enzipado->open('data/updates/last_update_admin_pro.zip');
          $extraido = $enzipado->extractTo("./");
          if($extraido == TRUE){
            for($x = 0; $x < $enzipado->numFiles; $x++){
              $archivo = $enzipado->statIndex($x);
              echo 'Extraido: '.$archivo['name'].'</br>';
            }
            unlink('data/updates/last_update_admin_pro.zip');
            echo'<script>alert("'.$language->get_translate('update_error_1').'");window.location="'.url.'";</script>';
          }else{
            echo'<script>alert("'.$language->get_translate('update_error_2').'");window.location="'.url.'";</script>';
          }
        }else{
          echo'<script>alert("'.$language->get_translate('update_error_3').'");window.location="'.url.'update";</script>';
        }
      }else{
        echo'<script>alert("'.$language->get_translate('update_error_4').'");window.location="'.url.'update";</script>';
      }
    }
  }
