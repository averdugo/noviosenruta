<!DOCTYPE html>
<html lang="es" class="no-js">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Novios en Ruta</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <meta http-equiv="Cache-Control" content="no-cache">
        <meta HTTP-EQUIV="Expires" CONTENT="Tue, 01 Jan 1980 1:00:00 GMT">
        <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">

        <!-- GLOBAL MANDATORY STYLES -->
        <link rel="shortcut icon" href="img/logo.png" type="image/x-icon" />
        <link href="//fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!-- PAGE LEVEL PLUGIN STYLES -->
        <link href="css/animate.css" rel="stylesheet">
        <link href="vendor/swiper/css/swiper.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css"/>

        <!-- THEME STYLES -->
        <link href="css/layout.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/easy-autocomplete.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/stylere.css" rel="stylesheet" type="text/css"/>
        <link href="//fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
        


        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
