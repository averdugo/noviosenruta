<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  $session=new session();
  if(!$session->check(@$_SESSION['session_code'])){echo'<script>window.location="'.url.'";</script>';}else{
  $language=new language();
	$id=$_SESSION['user_id'];
	$class_user=new user();
	$class_sitedata=new site();
?>
    <style>
      .profile-card-with-stats .btn-float {padding: 8px 14px 4px 14px;}.profile-card-with-cover .card-profile-image{position: absolute;top: 130px;width: 100%;text-align: center;}.profile-card-with-cover .card-profile-image img.img-border {border: 5px solid #fff;}.profile-card-with-cover .profile-card-with-cover-content {padding-top: 4rem;}#user-profile .profile-with-cover .profil-cover-details {position: absolute;margin-top: -90px;}#user-profile .profile-with-cover .profil-cover-details .profile-image img.img-border {border: 5px solid #fff;}#user-profile .profile-with-cover .profil-cover-details .card-title {color: #FFFFFF;text-shadow: 1px 1px 4px #1B2942;}#user-profile .navbar-profile {margin-left: 130px;}
    </style>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
  <?php echo $class_sitedata->sitedata('hook_before_body');?>
  <?php include 'core/modules/nav-module.php';?>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row"></div>
      <div class="content-body">
        <div id="user-profile">
          <div class="row">
            <div class="col-xs-12">
              <div class="card profile-with-cover">
                <div class="card-img-top img-fluid bg-cover height-300" style="background: url('<?php echo res;?>icon/background.jpg') 50%;"></div>
                <div class="media profil-cover-details">
                  <div class="media-left pl-2 pt-2">
                    <a class="profile-image">
                      <img src="https://www.gravatar.com/avatar/<?php echo md5($_SESSION['user_mail']);?>?s=250" class="rounded-circle img-border height-100" alt="Card image">
                    </a>
                  </div>
                  <div class="media-body media-middle row">
                    <div class="col-xs-6">
                      <h3 class="anti_select card-title" id="user_names"><?php echo $class_user->userdata($id,'name');?> <?php echo $class_user->userdata($id,'surnames');?></h3>
                    </div>
                    <div class="col-xs-6 text-xs-right">
                      <button type="button" class="btn btn-danger hidden-xs-down" onclick="javascript:location.href='<?php echo url;?>account/profile'"><i class="ft-user"></i> <?php echo $language->get_translate('my_profile');?></button>
                      <div class="anti_select btn bg-blue bg-lighten-1 hidden-xs-down"><font color="white"><?php echo $language->get_translate('profile_joined');?> <b><?php echo strftime($language->get_translate('profile_joined_form'),strtotime($class_user->userdata($id,'created_at')));?></b></font></div>
                    </div>
                  </div>
                </div>
                <nav class="navbar navbar-light navbar-profile"><h4><br></h4></nav>
              </div>
            </div>
          </div>
        </div>
        <div class="row match-height">
      		<div class="col-md-12">
      			<div class="card">
      				<div class="card-header">
      					<h4 class="card-title anti_select" id="basic-layout-form-center"><?php echo $language->get_translate('active_sessions');?></h4>
      					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      					<div class="heading-elements">
      						<ul class="list-inline mb-0">
      							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
      						</ul>
      					</div>
      				</div>
              <div class="card-body">
                <div class="card-block">
                  <table class="table">
                    <thead>
                      <tr>
                        <td><b><?php echo $language->get_translate('session_location');?></b></td>
                        <td><b><?php echo $language->get_translate('session_date');?></b></td>
                        <td><b><?php echo $language->get_translate('session_action');?></b></td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $mysqli=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                      	mysqli_set_charset( $mysqli, 'utf8');
                        $mysqli->real_query("select * from ".DB_PREFIX."session where user_code='$id' and status=1");
                        $query=$mysqli->store_result();
                        $n_rows=$query->num_rows;
                        while ($row=$query->fetch_assoc()){
                      ?>
                      <tr id="<?php echo $row['session_code'];?>">
                        <td><img src="<?php echo res;?>flags/<?php echo $row['country'];?>.png" width="20px"> <?php echo $row['country'];?></td>
                        <td><?php echo $row['date'];?></td>
                        <td><?php if($row['session_code']!=$_SESSION['session_code']){?><button type="button" class="btn btn-danger btn-sm" onclick="close_session('<?php echo $row['session_code'];?>')"><i class="fa fa-sign-out"></i> <?php echo $language->get_translate('close_session');?> </button><?php }else{?> <?php echo $language->get_translate('actual_session');?> <?php } ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function close_session(session) {
      var block_ele = $(this).closest('.card-body');
      $(block_ele).block({
        message:'<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; <?php echo $language->get_translate('loading');?> ...</div>',
        overlayCSS:{
          backgroundColor:'#fff',
          opacity:0.8,
          cursor:'wait',
        },
        css:{
          border:0,
          padding:'10px 15px',
          color:'#fff',
          width:'auto',
          backgroundColor:'#333',
        }
      });
      $.ajax({
        url:"<?php echo url;?>?action=closesession",
        data:'session='+session,
        success:function(data){
          $(block_ele).unblock();
          if(data=="true"){
            row=document.getElementById(session);
            row.parentNode.removeChild(row);
            sweetAlert("OK","<?php echo $language->get_translate('ok_close_session');?>","success");
          }else{
            sweetAlert("Error",data,"error");
          }
        }
      });
    }
  </script>
<?php } ?>
