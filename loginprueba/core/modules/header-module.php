<?php
	if(@$_GET['action']){}else{
?>
<!doctype html>
<html>
<head>
<?php
	$class_sitedata=new site();
	echo $class_sitedata->sitedata('hook_before_header');
?>
	<meta charset="utf-8">
	<title id="html_site_title"><?php echo titlepage;?></title>
	<meta name="description" content="<?php echo description; ?>">
	<meta name="keywords" content="<?php echo keywords; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="<?php echo res;?>icon/icon.png">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>feather/style.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>bootstrap/css/bootstrap-extended.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/app.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/colors.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/vertical-menu.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/vertical-overlay-menu.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>pace/css/pace.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>prism/css/prism.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/palette-gradient.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>morris/css/morris.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>datatables/css/datatables.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>datatables/css/datatables_btp4.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>sweetalert/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="<?php echo res;?>styles/style.css">
	<script src="<?php echo res;?>jquery/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>tether/js/tether.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>blockui/js/blockui.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>jquery-matchheight/js/jquery-matchheight.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>unison/js/unison.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>jquery-sliding-menu/js/jquery-sliding-menu.js" type="text/javascript"></script>
	<script src="<?php echo res;?>screenfull/js/screenfull.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>raphael/js/raphael-min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>morris/js/morris.js" type="text/javascript"></script>
	<script src="<?php echo res;?>datatables/js/datatables.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>datatables/js/datatables_btp4.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>tinymce/jquery.tinymce.min.js" type="text/javascript"></script>
	<script src="<?php echo res;?>tinymce/tinymce.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		tinymce.init({
			selector:".tinymceditor",
			setup:function(editor){
				editor.on('change',function(){
					tinymce.triggerSave();
				});
			}
		});
	</script>
<?php
		echo $class_sitedata->sitedata('hook_after_header');
	}
?>
