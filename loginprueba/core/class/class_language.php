<?php
  /*
  * @package puzzle-web
  * @version 2.0
  */
  class language
  {
    public function active_language()
    {
      $class_sitedata=new site();
      if(isset($_SESSION['language'])){$language=$_SESSION['language'];}else{$language=$class_sitedata->sitedata("default_language");}
      return $language;
    }

    public function get_translate($data)
    {
      $class_sitedata=new site();
      if(isset($_SESSION['language'])){$language=$_SESSION['language'];}else{$language=$class_sitedata->sitedata("default_language");}
      include path.'/core/languages/'.$language.'/'.$language.'.php';
      return $lang[$data];
    }

    public function list_language()
    {
      $class_sitedata=new site();
      $return='';
      $dir=path.'/core/languages/';
      $languages=scandir($dir);
      for($i=3;$i<count($languages);$i++){
        include path.'/core/languages/'.$languages[$i].'/'.$languages[$i].'.php';
        $is_active='';
        if($languages[$i]==$_SESSION['language']){$is_active=' selected=""';}
        $return.='<option value="'.$languages[$i].'" '.$is_active.'>'.$lang['lang_name'].'</option>';
      }
      return $return;
    }

    public function list_language_settings()
    {
      $class_sitedata=new site();
      $return='';
      $dir=path.'/core/languages/';
      $languages=scandir($dir);
      for($i=3;$i<count($languages);$i++){
        include path.'/core/languages/'.$languages[$i].'/'.$languages[$i].'.php';
        $is_active='';
        if($languages[$i]==$class_sitedata->sitedata("default_language")){$is_active=' selected=""';}
        $return.='<option value="'.$languages[$i].'" '.$is_active.'>'.$lang['lang_name'].'</option>';
      }
      return $return;
    }

    public function dropdown_language()
    {
      $return='';
      $dir=path.'/core/languages/';
      $languages=scandir($dir);
      for($i=3;$i<count($languages);$i++){
        include path.'/core/languages/'.$languages[$i].'/'.$languages[$i].'.php';
        $return.='<a href="'.url.'?action=changelanguage&lang='.$lang['code'].'" class="dropdown-item"><img style="position:relative;display:inline-block;width:1.33333333em;line-height:1em" src="'.url.'core/languages/'.$lang['code'].'/'.$lang['code'].'.svg"> '.$lang['lang_name'].'</a>';
      }
      return $return;
    }

  }

  $language=new language();
